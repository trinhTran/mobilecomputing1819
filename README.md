# Project Mobile Computing WS1819

### Topic \#11: Simulating SMTP Protocol by REST API

#### Author: Trinh Tran
#### Matriculation Number: 1105425

#### Links:
+ Final Master Build: https://gitlab.com/trinhTran/mobilecomputing1819/-/jobs/artifacts/master/download?job=smtp-app

#### How to use:
+ Download the zip from above link
+ Extract it to get 'smtp-app' directory
+ CD into smtp-all directory 'cd smtp-app'
+ Then run 'bash start-server.sh' and 'bash start-client.sh'
+ read Manual.pdf or Manual.doc for extra information.