#!/bin/bash

MAIN_JAR=total-mobile-computing-ws1819.jar
MAIN_CLASS=mobile.computing.ws1819.server.StartRestServer

java -cp ${MAIN_JAR} ${MAIN_CLASS}