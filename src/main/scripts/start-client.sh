#!/bin/bash

MAIN_JAR=total-mobile-computing-ws1819.jar
MAIN_CLASS=mobile.computing.ws1819.client.StartRestClient

if [ "$1" = "-debugLog" ]; then
    java -cp ${MAIN_JAR} -Dsmtp.log.level=0 ${MAIN_CLASS}
else
    java -cp ${MAIN_JAR} ${MAIN_CLASS}
fi