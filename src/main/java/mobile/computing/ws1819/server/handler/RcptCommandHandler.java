package mobile.computing.ws1819.server.handler;

import javax.ws.rs.core.Response;

import mobile.computing.ws1819.shared.log.LogEvent;
import mobile.computing.ws1819.shared.log.Logger;
import mobile.computing.ws1819.server.dataobject.MailBox;
import mobile.computing.ws1819.shared.model.Message;
import mobile.computing.ws1819.shared.model.Session;
import mobile.computing.ws1819.shared.type.HttpStatus;
import mobile.computing.ws1819.shared.type.SmtpCommand;
import mobile.computing.ws1819.shared.type.SmtpStates;
import mobile.computing.ws1819.shared.type.SmtpStatus;
import mobile.computing.ws1819.server.dataobject.SmtpSessionHolder;

public class RcptCommandHandler implements SmtpCommandHandler {
	private static final Logger LOGGER = Logger.getLogger(RcptCommandHandler.class);

	@Override
	public Response processCommand(SmtpCommand command, Message requestMessage) {
		try {
			Message responseMessage = new Message.MessageBuilder()
					.setHttpStatus(HttpStatus.OK.getStatusCode())
					.setTimestamp(String.valueOf(System.currentTimeMillis())).build();

			Session userSession = SmtpSessionHolder.userSession.get(requestMessage.getSmtpSessionUID());

			String rcptMailbox = parseRecipentMailbox(requestMessage);
			if (MailBox.mailBoxExist(rcptMailbox)) {
				if (userSession.tryToAddNewRecipient(rcptMailbox)) {
					responseMessage.setSmtpStatus(SmtpStatus._250.getCode());
					responseMessage.setSmtpMessageContent(SmtpStatus._250.getStdMessage());
					responseMessage.setCurrentSmtpStateOrder(SmtpStates.RECEIVING_RECIPENT.getStateOrder());

					userSession.setCurrentSmtpState(SmtpStates.RECEIVING_RECIPENT);
				} else {
					responseMessage.setSmtpStatus(SmtpStatus._452.getCode());
					responseMessage.setSmtpMessageContent(SmtpStatus._452.getStdMessage());
					responseMessage.setCurrentSmtpStateOrder(userSession.getCurrentSmtpState().getStateOrder());
				}
			} else {
				responseMessage.setSmtpStatus(SmtpStatus._551.getCode());
				responseMessage.setSmtpMessageContent(SmtpStatus._551.getStdMessage());
				responseMessage.setCurrentSmtpStateOrder(userSession.getCurrentSmtpState().getStateOrder());
			}

			return Response.status(HttpStatus.OK).entity(responseMessage.toJsonString()).build();
		} catch (Exception e) {
			LOGGER.error(LogEvent.create("processCommand", "error while processing " + command.getName() + " response message object")
					.add(e));

			return Response.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	private String parseRecipentMailbox(Message message) {
		String content = message.getSmtpMessageContent();
		String clientMailBox = content.substring(content.indexOf(":<") + ":<".length(), content.lastIndexOf(">"));
		return clientMailBox;
	}
}
