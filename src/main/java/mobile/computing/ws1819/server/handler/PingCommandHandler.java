package mobile.computing.ws1819.server.handler;

import javax.ws.rs.core.Response;

import mobile.computing.ws1819.shared.log.LogEvent;
import mobile.computing.ws1819.shared.log.Logger;
import mobile.computing.ws1819.shared.model.Message;
import mobile.computing.ws1819.shared.model.Session;
import mobile.computing.ws1819.shared.type.HttpStatus;
import mobile.computing.ws1819.shared.type.SmtpCommand;
import mobile.computing.ws1819.shared.type.SmtpStates;
import mobile.computing.ws1819.shared.type.SmtpStatus;
import mobile.computing.ws1819.server.dataobject.SmtpSessionHolder;

import java.util.UUID;

public class PingCommandHandler implements SmtpCommandHandler {
	private static final Logger LOGGER = Logger.getLogger(PingCommandHandler.class);

	@Override
	public Response processCommand(SmtpCommand command, Message requestMessage) {
		LOGGER.info(LogEvent.create("processCommand", "received a request to " + command.getName()
				+ " command. Will try to create new session for this client").add("smtpCommand", command.getName()));

		try {
			/*
			When the client send a request to PING command, we will create a new SMTP session for it
			by creating a new session object with a unique uuid.
			Then we update the state table inside the session object to session SESSION_INITIALIED
			*/
			Session session = createNewSmtpSession();
			SmtpSessionHolder.userSession.putIfAbsent(session.getUuid(), session);

			Message responseMessage = new Message.MessageBuilder()
					.setSmtpStatus(SmtpStatus._220.getCode())
					.setHttpStatus(HttpStatus.OK.getStatusCode())
					.setSmtpSessionUID(session.getUuid())
					.setTimestamp(String.valueOf(System.currentTimeMillis()))
					.setSmtpMessageContent(SmtpStatus._220.toString())
					.setCurrentSmtpStateOrder(session.getCurrentSmtpState().getStateOrder()).build();

			return Response.status(HttpStatus.OK).entity(responseMessage.toJsonString()).build();
		} catch (Exception e) {
			LOGGER.error(LogEvent.create("processCommand", "error while processing " + command.getName() + " response message object")
					.add(e));

			return Response.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	private Session createNewSmtpSession() {
		Session newSession = new Session();
		newSession.setCurrentSmtpState(SmtpStates.SESSION_INITIALIED);
		newSession.setUuid(UUID.randomUUID().toString());
		return newSession;
	}
}