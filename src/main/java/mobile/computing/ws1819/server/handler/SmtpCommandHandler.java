package mobile.computing.ws1819.server.handler;

import mobile.computing.ws1819.shared.model.Message;
import mobile.computing.ws1819.shared.type.SmtpCommand;

import javax.ws.rs.core.Response;

/*
An implementation of SmtpCommandHandler will first do a syntax validation.
Then it will try to process the command with provided message.
*/
public interface SmtpCommandHandler {
	/*
	SmtpApiGateway will route the request to here and require an HTTP respone back. 
	This method will try process the request and construct an HTTP respone.
	*/
	Response processCommand(SmtpCommand command, Message requestMessage);
}
