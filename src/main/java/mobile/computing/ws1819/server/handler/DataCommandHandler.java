package mobile.computing.ws1819.server.handler;

import javax.ws.rs.core.Response;

import mobile.computing.ws1819.shared.log.LogEvent;
import mobile.computing.ws1819.shared.log.Logger;
import mobile.computing.ws1819.shared.model.Message;
import mobile.computing.ws1819.shared.model.Session;
import mobile.computing.ws1819.shared.constant.ServerLimits;
import mobile.computing.ws1819.shared.type.HttpStatus;
import mobile.computing.ws1819.shared.type.SmtpCommand;
import mobile.computing.ws1819.shared.type.SmtpStates;
import mobile.computing.ws1819.shared.type.SmtpStatus;
import mobile.computing.ws1819.server.dataobject.SmtpSessionHolder;

import static mobile.computing.ws1819.shared.constant.SharedConst.CRLF;
import static mobile.computing.ws1819.shared.constant.SharedConst.NEW_LINE;

public class DataCommandHandler implements SmtpCommandHandler{
	private static final Logger LOGGER = Logger.getLogger(DataCommandHandler.class);

	@Override
	public Response processCommand(SmtpCommand command, Message requestMessage) {
		try {
			Message responseMessage = new Message.MessageBuilder()
					.setHttpStatus(HttpStatus.OK.getStatusCode())
					.setTimestamp(String.valueOf(System.currentTimeMillis())).build();

			Session userSession = SmtpSessionHolder.userSession.get(requestMessage.getSmtpSessionUID());
			SmtpStates currentSessionState = userSession.getCurrentSmtpState();

			if (currentSessionState.equals(SmtpStates.RECEIVING_RECIPENT)) { // it is at stage 1
				responseMessage.setSmtpStatus(SmtpStatus._354.getCode());
				responseMessage.setSmtpMessageContent(SmtpStatus._354.getStdMessage());
				responseMessage.setCurrentSmtpStateOrder(SmtpStates.RECEIVING_DATA.getStateOrder());

				userSession.setCurrentSmtpState(SmtpStates.RECEIVING_DATA);
			} 
			
			if (currentSessionState.equals(SmtpStates.RECEIVING_DATA)) { // now it must be stage 2
				if (overDataLimit(requestMessage)) { // checking if we are ddos-ed
					responseMessage.setSmtpStatus(SmtpStatus._552.getCode());
					responseMessage.setSmtpMessageContent(SmtpStatus._552.getStdMessage()+". Current mail transaction is terminated.");

				} else {
					responseMessage.setSmtpStatus(SmtpStatus._250.getCode());
					responseMessage.setSmtpMessageContent(SmtpStatus._250.getStdMessage());
					String fromLine = tryToGetAndRemoveFromLine(requestMessage);
					String toLine = tryToGetAndRemoveToLine(requestMessage);
					String subjectLine = tryToGetAndRemoveSubjectLine(requestMessage);
					String mailBody = tryToGetMailBody(requestMessage);
					userSession.setFromLine(fromLine);
					userSession.setToLine(toLine);
					userSession.setSubjectLine(subjectLine);
					userSession.setMailBody(mailBody);

					LOGGER.info(LogEvent.create("processCommand", "now sending the mail")
							.add("from", userSession.getReverseMailBox())
							.add("to", userSession.getRecipentList().toArray())
							.add("subject", userSession.getSubjectLine())
							.add("mailBody", NEW_LINE + userSession.getMailBody()));

					sendMail(userSession);
				}

				//After finished we will clean up the session (not delete) and change state back to CLIENT_SAID_HELEO
				responseMessage.setCurrentSmtpStateOrder(SmtpStates.CLIENT_SAID_HELO.getStateOrder());
				userSession.setCurrentSmtpState(SmtpStates.CLIENT_SAID_HELO);
				userSession.cleanUp();
			}

			return Response.status(HttpStatus.OK).entity(responseMessage.toJsonString()).build();
		} catch (Exception e) {
			LOGGER.error(LogEvent.create("processCommand", "error while processing " + command.getName() + " response message object")
					.add(e));

			return Response.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	private boolean overDataLimit(Message message) {
		String content = message.getSmtpMessageContent();
		if (content.length() > ServerLimits.MESSAGE_CONTENT_SIZE.getUpperBound()) {
			return true;
		}
		return false;
	}
	
	private String tryToGetAndRemoveFromLine(Message message) {
		String content = message.getSmtpMessageContent();
		String[] contentLine = content.split(CRLF);
		for (String line : contentLine) {
			if (line.startsWith("From:") || line.startsWith("from:") || line.startsWith("FROM:")) {
				message.setSmtpMessageContent(content.replaceAll(line+CRLF, ""));
				return line;
			}
		}
		return "";
	}

	private String tryToGetAndRemoveToLine(Message message) {
		String content = message.getSmtpMessageContent();
		String[] contentLine = content.split(CRLF);
		for (String line : contentLine) {
			if (line.startsWith("To:") || line.startsWith("to:") || line.startsWith("TO:")) {
				message.setSmtpMessageContent(content.replaceAll(line+CRLF, ""));
				return line;
			}
		}
		return "";
	}

	private String tryToGetAndRemoveSubjectLine(Message message) {
		String content = message.getSmtpMessageContent();
		String[] contentLine = content.split(CRLF);
		for (String line : contentLine) {
			if (line.startsWith("Subject:") || line.startsWith("subject:") || line.startsWith("SUBJECT:")) {
				message.setSmtpMessageContent(content.replaceAll(line+CRLF, ""));
				return line;
			}
		}
		return null;
	}

	private String tryToGetMailBody(Message message) {
		String content = message.getSmtpMessageContent();
		return content;
	}

	private void sendMail(Session userSession) {
	}
}
