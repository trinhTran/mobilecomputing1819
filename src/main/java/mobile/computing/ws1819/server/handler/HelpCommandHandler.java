package mobile.computing.ws1819.server.handler;

import static mobile.computing.ws1819.shared.constant.SharedConst.CRLF;
import javax.ws.rs.core.Response;

import mobile.computing.ws1819.server.dataobject.SmtpSessionHolder;
import mobile.computing.ws1819.shared.log.LogEvent;
import mobile.computing.ws1819.shared.log.Logger;
import mobile.computing.ws1819.shared.constant.HelpMessages;
import mobile.computing.ws1819.shared.model.Message;
import mobile.computing.ws1819.shared.model.Session;
import mobile.computing.ws1819.shared.type.HttpStatus;
import mobile.computing.ws1819.shared.type.SmtpCommand;
import mobile.computing.ws1819.shared.type.SmtpStatus;

public class HelpCommandHandler implements SmtpCommandHandler{
	private static final Logger LOGGER = Logger.getLogger(HelpCommandHandler.class);

	@Override
	public Response processCommand(SmtpCommand command, Message requestMessage) {
		LOGGER.info(LogEvent.create("processCommand", "received a request to " + command.getName() + " command")
				.add("smtpCommand", command.getName())
				.add("message", requestMessage.toString()));
		try {
			Message responseMessage = new Message.MessageBuilder()
					.setHttpStatus(HttpStatus.OK.getStatusCode())
					.setTimestamp(String.valueOf(System.currentTimeMillis())).build();

			Session userSession = SmtpSessionHolder.userSession.get(requestMessage.getSmtpSessionUID());

			String helpCommandParam = parseHelpCommandParam(requestMessage);
			SmtpCommand smtpCommand = SmtpCommand.getByName(helpCommandParam);
			if (smtpCommand != null) {
				responseMessage.setSmtpStatus(SmtpStatus._214.getCode());
				responseMessage.setSmtpMessageContent(generateSmtpCommandHelpMessage(smtpCommand));
				responseMessage.setCurrentSmtpStateOrder(userSession.getCurrentSmtpState().getStateOrder());
			} else {
				responseMessage.setSmtpStatus(SmtpStatus._211.getCode());
				responseMessage.setSmtpMessageContent(generateGeneralHelpMessage());
				responseMessage.setCurrentSmtpStateOrder(userSession.getCurrentSmtpState().getStateOrder());
			}

			return Response.status(HttpStatus.OK).entity(responseMessage.toJsonString()).build();
		} catch (Exception e) {
			LOGGER.error(LogEvent.create("processCommand", "error while processing " + command.getName() + " response message object")
					.add(e));

			return Response.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	private String parseHelpCommandParam(Message message) {
		String content = message.getSmtpMessageContent();
		String param = content.substring(SmtpCommand.HELP.name.length(), content.lastIndexOf(CRLF));
		return param.trim();
	}
	
	private String generateGeneralHelpMessage() {
		return generateHelpMessage(HelpMessages.GENERAL, SmtpStatus._211);
	}
	
	private String generateSmtpCommandHelpMessage(SmtpCommand smtpCommand) {
		return generateHelpMessage(smtpCommand.getHelpMessage(), SmtpStatus._214);
	}
	
	private String generateHelpMessage(String message, SmtpStatus status) {
		StringBuilder builder = new StringBuilder();
		String[] helpMessageLines = message.split(System.lineSeparator());
		for (int i = 0; i < helpMessageLines.length; i++) {
			builder.append(SmtpStatus.generateMessageWithStatus(helpMessageLines[i], status, (i == helpMessageLines.length-1) ? true : false));
			builder.append(System.lineSeparator());
		}
		return builder.toString();
	}
}
