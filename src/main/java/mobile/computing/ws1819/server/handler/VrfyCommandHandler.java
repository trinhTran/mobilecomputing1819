package mobile.computing.ws1819.server.handler;

import javax.ws.rs.core.Response;

import mobile.computing.ws1819.server.dataobject.SmtpSessionHolder;
import mobile.computing.ws1819.shared.log.LogEvent;
import mobile.computing.ws1819.shared.log.Logger;
import mobile.computing.ws1819.server.dataobject.MailBox;
import mobile.computing.ws1819.shared.model.Message;
import mobile.computing.ws1819.shared.model.Session;
import mobile.computing.ws1819.shared.type.HttpStatus;
import mobile.computing.ws1819.shared.type.SmtpCommand;
import mobile.computing.ws1819.shared.type.SmtpStatus;

import static mobile.computing.ws1819.shared.constant.SharedConst.CRLF;

import java.util.List;
import java.util.Map;

public class VrfyCommandHandler implements SmtpCommandHandler {
	private static final Logger LOGGER = Logger.getLogger(VrfyCommandHandler.class);

	@Override
	public Response processCommand(SmtpCommand command, Message requestMessage) {
		LOGGER.info(LogEvent.create("processCommand", "received a request to " + command.getName() + " command")
				.add("smtpCommand", command.getName())
				.add("message", requestMessage.toString()));
		try {
			Message responseMessage = new Message.MessageBuilder()
					.setHttpStatus(HttpStatus.OK.getStatusCode())
					.setTimestamp(String.valueOf(System.currentTimeMillis())).build();

			Session userSession = SmtpSessionHolder.userSession.get(requestMessage.getSmtpSessionUID());

			String commandStringParam = parseVrfyCommandStringParam(requestMessage);
			if (MailBox.isMailingListExist(commandStringParam)) {
				responseMessage.setSmtpStatus(SmtpStatus._550.getCode());
				responseMessage.setSmtpMessageContent(SmtpStatus.generateMessageWithStatus("That is a mailing list, not a user.", SmtpStatus._550, true));
				responseMessage.setCurrentSmtpStateOrder(userSession.getCurrentSmtpState().getStateOrder());
			} else {
				String userMailBoxName = MailBox.getUserMailBoxName(commandStringParam);
				if (userMailBoxName != null && !userMailBoxName.isEmpty()) { 
					// in this case commandStringParam must be an mailbox address
					responseMessage.setSmtpStatus(SmtpStatus._250.getCode());
					String returnMessage = userMailBoxName + " <" + commandStringParam + ">";
					responseMessage.setSmtpMessageContent(SmtpStatus.generateMessageWithStatus(returnMessage, SmtpStatus._250, true));
					responseMessage.setCurrentSmtpStateOrder(userSession.getCurrentSmtpState().getStateOrder());
				} else {
					//Now if commandStringParam is not an mailbox address, then we will assume it is a user mailbox name
					//We gonna search for that user name
					List<Map.Entry<String, String>> searchResult = MailBox.searchMailboxByName(commandStringParam);
					if (searchResult != null && !searchResult.isEmpty()) {
						if (searchResult.size() == 1) { // incase only one mail box is found, we should response 250 message
							responseMessage.setSmtpStatus(SmtpStatus._250.getCode());
							String returnMessage = searchResult.get(0).getValue() + " <" + searchResult.get(0).getKey() + ">";
							responseMessage.setSmtpMessageContent(SmtpStatus.generateMessageWithStatus(returnMessage, SmtpStatus._250, true));
							responseMessage.setCurrentSmtpStateOrder(userSession.getCurrentSmtpState().getStateOrder());
						} else {
							responseMessage.setSmtpStatus(SmtpStatus._553.getCode());
							responseMessage.setSmtpMessageContent(buildAmbiguousAnswer(searchResult));
							responseMessage.setCurrentSmtpStateOrder(userSession.getCurrentSmtpState().getStateOrder());
						}
					} else {
						responseMessage.setSmtpStatus(SmtpStatus._551.getCode());
						responseMessage.setSmtpMessageContent(SmtpStatus._551.getStdMessage());
						responseMessage.setCurrentSmtpStateOrder(userSession.getCurrentSmtpState().getStateOrder());
					}
				}
			}

			return Response.status(HttpStatus.OK).entity(responseMessage.toJsonString()).build();
		} catch (Exception e) {
			LOGGER.error(LogEvent.create("processCommand", "error while processing " + command.getName() + " response message object")
					.add(e));

			return Response.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	private String parseVrfyCommandStringParam(Message message) {
		String content = message.getSmtpMessageContent();
		String param = content.substring(SmtpCommand.VRFY.name.length(), content.lastIndexOf(CRLF));
		return param.trim();
	}
	
	private String buildAmbiguousAnswer(List<Map.Entry<String, String>> mailBoxList) {
		StringBuilder builder = new StringBuilder();
		builder.append(SmtpStatus.generateMessageWithStatus(" Ambiguous; Possibilities are", SmtpStatus._553, false)).append(System.lineSeparator());
		for (int i = 0; i < mailBoxList.size(); i++) {
			Map.Entry<String, String> mailbox = mailBoxList.get(i);
			builder.append(SmtpStatus.generateMessageWithStatus(mailbox.getValue()+" <"+mailbox.getKey()+">", SmtpStatus._553, (i == mailBoxList.size()-1) ? true : false));
			builder.append(System.lineSeparator());
		}
		return builder.toString();
	}
}
