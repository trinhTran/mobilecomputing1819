package mobile.computing.ws1819.server.handler;

import javax.ws.rs.core.Response;

import mobile.computing.ws1819.shared.log.LogEvent;
import mobile.computing.ws1819.shared.log.Logger;
import mobile.computing.ws1819.shared.model.Message;
import mobile.computing.ws1819.shared.model.Session;
import mobile.computing.ws1819.shared.type.HttpStatus;
import mobile.computing.ws1819.shared.type.SmtpCommand;
import mobile.computing.ws1819.shared.type.SmtpStates;
import mobile.computing.ws1819.shared.type.SmtpStatus;
import mobile.computing.ws1819.server.dataobject.SmtpSessionHolder;

import static mobile.computing.ws1819.shared.constant.SharedConst.CRLF;
import static mobile.computing.ws1819.shared.constant.SharedConst.SP;

public class HeloCommandHandler implements SmtpCommandHandler{
	private static final Logger LOGGER = Logger.getLogger(HeloCommandHandler.class);

	@Override
	public Response processCommand(SmtpCommand command, Message requestMessage) {
		LOGGER.info(LogEvent.create("processCommand", "received a request to " + command.getName() + " command")
				.add("smtpCommand", command.getName())
				.add("message", requestMessage.toString()));
		try {
			Message responseMessage = new Message.MessageBuilder()
					.setHttpStatus(HttpStatus.OK.getStatusCode())
					.setTimestamp(String.valueOf(System.currentTimeMillis())).build();

			Session userSession = SmtpSessionHolder.userSession.get(requestMessage.getSmtpSessionUID());
			userSession.cleanUp();
			
			responseMessage.setSmtpStatus(SmtpStatus._250.getCode());
			responseMessage.setSmtpMessageContent(SmtpStatus._250.getStdMessage());
			responseMessage.setCurrentSmtpStateOrder(SmtpStates.CLIENT_SAID_HELO.getStateOrder());

			userSession.setCurrentSmtpState(SmtpStates.CLIENT_SAID_HELO);
			userSession.setClientDomain(parseClientDomain(requestMessage));
			
			return Response.status(HttpStatus.OK).entity(responseMessage.toJsonString()).build();
		} catch (Exception e) {
			LOGGER.error(LogEvent.create("processCommand", "error while processing " + command.getName() + " response message object")
					.add(e));

			return Response.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	/*
	try to extract client domain in SMTP message.
	*/
	private String parseClientDomain(Message message) {
		String smtpConent = message.getSmtpMessageContent();
		String domain = smtpConent.substring(SmtpCommand.HELO.name.length()+ SP.length(), smtpConent.indexOf(CRLF));
		return domain;
	}
}
