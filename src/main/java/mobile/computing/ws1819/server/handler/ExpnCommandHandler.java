package mobile.computing.ws1819.server.handler;

import static mobile.computing.ws1819.shared.constant.SharedConst.CRLF;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;

import mobile.computing.ws1819.server.dataobject.SmtpSessionHolder;
import mobile.computing.ws1819.shared.log.LogEvent;
import mobile.computing.ws1819.shared.log.Logger;
import mobile.computing.ws1819.server.dataobject.MailBox;
import mobile.computing.ws1819.shared.model.Message;
import mobile.computing.ws1819.shared.model.Session;
import mobile.computing.ws1819.shared.type.HttpStatus;
import mobile.computing.ws1819.shared.type.SmtpCommand;
import mobile.computing.ws1819.shared.type.SmtpStatus;

public class ExpnCommandHandler implements SmtpCommandHandler{
	private static final Logger LOGGER = Logger.getLogger(ExpnCommandHandler.class);

	@Override
	public Response processCommand(SmtpCommand command, Message requestMessage) {
		LOGGER.info(LogEvent.create("processCommand", "received a request to " + command.getName() + " command")
				.add("smtpCommand", command.getName())
				.add("message", requestMessage.toString()));
		try {
			Message responseMessage = new Message.MessageBuilder()
					.setHttpStatus(HttpStatus.OK.getStatusCode())
					.setTimestamp(String.valueOf(System.currentTimeMillis())).build();

			Session userSession = SmtpSessionHolder.userSession.get(requestMessage.getSmtpSessionUID());

			String commandStringParam = parseExpnCommandStringParam(requestMessage);
			if (MailBox.isMailingListExist(commandStringParam)) { //if mailing list exist we will send back 250 message
				List<Map.Entry<String, String>> mailingListMailBoxes = MailBox.getMailingList(commandStringParam);

				responseMessage.setSmtpStatus(SmtpStatus._250.getCode());
				responseMessage.setSmtpMessageContent(buildMailingListSuccessAnswer(mailingListMailBoxes));
				responseMessage.setCurrentSmtpStateOrder(userSession.getCurrentSmtpState().getStateOrder());
			} else {
				String userMailBoxName = MailBox.getUserMailBoxName(commandStringParam);
				List<Map.Entry<String, String>> searchResult = MailBox.searchMailboxByName(commandStringParam);
				if ((userMailBoxName != null && !userMailBoxName.isEmpty())
						|| (searchResult != null && !searchResult.isEmpty())) { // in this case commandStringParam must be an user

					responseMessage.setSmtpStatus(SmtpStatus._550.getCode());
					responseMessage.setSmtpMessageContent(SmtpStatus.generateMessageWithStatus("That is a user, not a mailing list.", SmtpStatus._550, true));
					responseMessage.setCurrentSmtpStateOrder(userSession.getCurrentSmtpState().getStateOrder());
				} else {
					responseMessage.setSmtpStatus(SmtpStatus._550.getCode());
					responseMessage.setSmtpMessageContent(SmtpStatus._550.getStdMessage());
					responseMessage.setCurrentSmtpStateOrder(userSession.getCurrentSmtpState().getStateOrder());
				}
			}

			return Response.status(HttpStatus.OK).entity(responseMessage.toJsonString()).build();
		} catch (Exception e) {
			LOGGER.error(LogEvent.create("processCommand", "error while processing " + command.getName() + " response message object")
					.add(e));

			return Response.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	//extract the parameter that is sent together with EXPN command
	private String parseExpnCommandStringParam(Message message) {
		String content = message.getSmtpMessageContent();
		String param = content.substring(SmtpCommand.EXPN.name.length(), content.lastIndexOf(CRLF));
		return param.trim();
	}

	//response of EXPN command is multiple-lined. Therefore we will construct that here.
	private String buildMailingListSuccessAnswer(List<Map.Entry<String, String>> mailingListMailBoxes) {
		StringBuilder builder = new StringBuilder();
		for (Iterator<Map.Entry<String, String>> it = mailingListMailBoxes.iterator(); it.hasNext(); ) {
			Map.Entry<String, String> mailbox = it.next();
			builder.append(SmtpStatus.generateMessageWithStatus(mailbox.getValue()+" <"+mailbox.getKey()+">", SmtpStatus._250, (!it.hasNext()) ? true : false));
			builder.append(System.lineSeparator());
		}
		return builder.toString();
	}
}
