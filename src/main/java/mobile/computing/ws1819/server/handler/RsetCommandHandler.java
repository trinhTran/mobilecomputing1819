package mobile.computing.ws1819.server.handler;

import javax.ws.rs.core.Response;

import mobile.computing.ws1819.shared.log.LogEvent;
import mobile.computing.ws1819.shared.log.Logger;
import mobile.computing.ws1819.shared.model.Message;
import mobile.computing.ws1819.shared.model.Session;
import mobile.computing.ws1819.shared.type.HttpStatus;
import mobile.computing.ws1819.shared.type.SmtpCommand;
import mobile.computing.ws1819.shared.type.SmtpStates;
import mobile.computing.ws1819.shared.type.SmtpStatus;
import mobile.computing.ws1819.server.dataobject.SmtpSessionHolder;

public class RsetCommandHandler implements SmtpCommandHandler {
	private static final Logger LOGGER = Logger.getLogger(RsetCommandHandler.class);

	@Override
	public Response processCommand(SmtpCommand command, Message requestMessage) {
		try {
			Message responseMessage = new Message.MessageBuilder()
					.setHttpStatus(HttpStatus.OK.getStatusCode())
					.setTimestamp(String.valueOf(System.currentTimeMillis())).build();

			Session userSession = SmtpSessionHolder.userSession.get(requestMessage.getSmtpSessionUID());
			SmtpStates currentState = userSession.getCurrentSmtpState();

			responseMessage.setSmtpStatus(SmtpStatus._250.getCode());
			responseMessage.setSmtpMessageContent(SmtpStatus._250.getStdMessage());

			/*
			If client haven't sent HELO or EHLO yet, then we will stay at the current state
			If client have already sent HELO or EHLO, then RSET will clean up the session and
			set the state back to the point right after HELO or EHLO is issued.
			 */
			responseMessage.setCurrentSmtpStateOrder(currentState.getStateOrder());
			if (currentState.getStateOrder() >= SmtpStates.CLIENT_SAID_HELO.getStateOrder()) {
				userSession.cleanUp();
				responseMessage.setCurrentSmtpStateOrder(SmtpStates.CLIENT_SAID_HELO.getStateOrder());
				userSession.setCurrentSmtpState(SmtpStates.CLIENT_SAID_HELO);
			}

			return Response.status(HttpStatus.OK).entity(responseMessage.toJsonString()).build();
		} catch (Exception e) {
			LOGGER.error(LogEvent.create("processCommand", "error while processing " + command.getName() + " response message object")
					.add(e));

			return Response.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}
}
