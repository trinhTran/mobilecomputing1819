package mobile.computing.ws1819.server.handler;

import javax.ws.rs.core.Response;

import mobile.computing.ws1819.shared.log.LogEvent;
import mobile.computing.ws1819.shared.log.Logger;
import mobile.computing.ws1819.shared.model.Message;
import mobile.computing.ws1819.shared.model.Session;
import mobile.computing.ws1819.shared.type.HttpStatus;
import mobile.computing.ws1819.shared.type.SmtpCommand;
import mobile.computing.ws1819.shared.type.SmtpStates;
import mobile.computing.ws1819.shared.type.SmtpStatus;
import mobile.computing.ws1819.server.dataobject.SmtpSessionHolder;

import java.net.Inet4Address;
import java.net.UnknownHostException;

import static mobile.computing.ws1819.shared.constant.SharedConst.CRLF;
import static mobile.computing.ws1819.shared.constant.ServerLimits.MESSAGE_CONTENT_SIZE;
import static mobile.computing.ws1819.shared.constant.SharedConst.SP;

public class EhloCommandHandler implements SmtpCommandHandler{
	private static final Logger LOGGER = Logger.getLogger(EhloCommandHandler.class);

	@Override
	public Response processCommand(SmtpCommand command, Message requestMessage) {
		try {
			Message responseMessage = new Message.MessageBuilder()
					.setHttpStatus(HttpStatus.OK.getStatusCode())
					.setTimestamp(String.valueOf(System.currentTimeMillis())).build();

			Session userSession = SmtpSessionHolder.userSession.get(requestMessage.getSmtpSessionUID());
			userSession.cleanUp();

			String clientDomain = parseClientDomain(requestMessage);
			userSession.setCurrentSmtpState(SmtpStates.CLIENT_SAID_HELO);
			userSession.setClientDomain(clientDomain);

			responseMessage.setSmtpStatus(SmtpStatus._250.getCode());
			responseMessage.setSmtpMessageContent(getEhloResponse(clientDomain));
			responseMessage.setCurrentSmtpStateOrder(SmtpStates.CLIENT_SAID_HELO.getStateOrder());

			return Response.status(HttpStatus.OK).entity(responseMessage.toJsonString()).build();
		} catch (Exception e) {
			LOGGER.error(LogEvent.create("processCommand", "error while processing " + command.getName() + " response message object")
					.add(e));

			return Response.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	//try to extract client domain from the message content
	private String parseClientDomain(Message message) {
		String content = message.getSmtpMessageContent();
		String clientDomain = content.substring(SmtpCommand.EHLO.name.length()+ SP.length(), content.indexOf(CRLF));
		return clientDomain;
	}

	/*
	EHLO response message is abit more complicated then HELO command.
	We will construct it here with multiple line response.
	*/
	private String getEhloResponse(String userDomain) {
		userDomain = userDomain.replaceAll("qnguyen(-pc)?","trinhtran");
		StringBuilder builder = new StringBuilder();
		builder.append("250-").append("server MC-WS1819 ESMTP").append(" greets ").append(userDomain).append(System.lineSeparator());
		builder.append("250-").append("SIZE ").append(MESSAGE_CONTENT_SIZE.getUpperBound()).append(System.lineSeparator());
		if (SmtpCommand.EXPN.isEnabled()) {
			builder.append("250-").append("EXPN").append(System.lineSeparator());
		}
		if (SmtpCommand.VRFY.isEnabled()) {
			builder.append("250-").append("VRFY").append(System.lineSeparator());
		}
		if (SmtpCommand.HELP.isEnabled()) {
			builder.append("250-").append("HELP").append(System.lineSeparator());
		}
		builder.append("250 ").append("SMTPUTF8").append(System.lineSeparator());
		return builder.toString();
	}
}