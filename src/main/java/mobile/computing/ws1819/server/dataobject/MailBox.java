package mobile.computing.ws1819.server.dataobject;

import mobile.computing.ws1819.shared.log.LogEvent;
import mobile.computing.ws1819.shared.log.Logger;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static mobile.computing.ws1819.shared.constant.SharedConst.CONF_DIR;

/*
This class is just a holder class for user's mail boxes object and mailing list object.
*/
public class MailBox {
	private static final Logger LOGGER = Logger.getLogger(MailBox.class);

	private static final File mailboxFile = new File(CONF_DIR, "mailbox.properties");
	private static final File mailingListFile = new File(CONF_DIR, "mailinglist.properties");

	// mailbox include mailbox address and users name
	public static final ConcurrentHashMap<String, String> userMailBoxes = new ConcurrentHashMap<>();
	
	//mailingList contains multiple mail boxes
	//This will map each mailing list name to a list of mailboxes
	public static final ConcurrentHashMap<String, List<Map.Entry<String, String>>> mailingList = new ConcurrentHashMap<>();

	public static void addUserMailBox(String mailboxAddress, String userName) {
		userMailBoxes.putIfAbsent(mailboxAddress, userName);
	}

	/*
	listing all the mail box, whose user name matching with the input pattern.
	*/
	public static List<Map.Entry<String, String>> searchMailboxByName(String userNamePattern) {
		List<Map.Entry<String, String>> result = new ArrayList<>();
		for (Map.Entry<String, String> mailBoxEntry : userMailBoxes.entrySet()) {
			if (mailBoxEntry.getValue().toLowerCase().contains(userNamePattern.toLowerCase())) {
				result.add(mailBoxEntry);
			}
		}
		return result;
	}

	//get the user name base on mail box address
	public static String getUserMailBoxName(String mailBoxAddress) {
		if (mailBoxExist(mailBoxAddress)) {
			return userMailBoxes.get(mailBoxAddress);
		}
		return null;
	}

	//check if the mail box address exist
	public static boolean mailBoxExist(String mailBoxAddress) {
		return userMailBoxes.containsKey(mailBoxAddress);
	}

	//get the list of mailboxes that belong to a mailing list
	public static List<Map.Entry<String, String>> getMailingList(String mailingListName) {
		if (isMailingListExist(mailingListName)) {
			return mailingList.get(mailingListName);
		}
		return null;
	}

	//check if mailing list exist
	public static boolean isMailingListExist(String mailingListName) {
		return mailingList.containsKey(mailingListName);
	}

	/*
	read mail box and mailing list config from the configs folder.
	*/
	public static void initialize() throws IOException {
		readMailboxConfig();
		readMailinglistConfig();
	}

	public static void readMailboxConfig() throws IOException {
		Properties mailboxConf = new Properties();
		try (InputStream is = new FileInputStream(mailboxFile)) {
			mailboxConf.load(is);
			mailboxConf.keySet().parallelStream().forEach(key -> {
				addUserMailBox((String) key, mailboxConf.getProperty((String) key));
			});
		} catch (IOException e) {
			LOGGER.error(LogEvent.create("readMailboxConfig", "cannot read mail box config")
				.add("path", mailboxFile.getAbsolutePath())
				.add(e));
			throw new IOException("cannot read mail box config file");
		}
	}

	public static void readMailinglistConfig() throws IOException {
		Properties mailingListConf = new Properties();
		try (InputStream is = new FileInputStream(mailingListFile)) {
			mailingListConf.load(is);
			mailingListConf.keySet().parallelStream().forEach(key -> {
				String mailingListName = (String) key;
				ArrayList mailAdresses = new ArrayList(Arrays.asList(mailingListConf.getProperty(mailingListName).split(",")));
				mailingList.putIfAbsent(mailingListName, userMailBoxes.entrySet().parallelStream().filter(entry -> {
					return mailAdresses.contains(entry.getKey());
				}).collect(Collectors.toList()));
			});
		} catch (IOException e) {
			LOGGER.error(LogEvent.create("readMailinglistConfig", "cannot read mailing list config")
					.add("path", mailingListFile.getAbsolutePath())
					.add(e));
			throw new IOException("cannot read mailing list config file");
		}
	}
}
