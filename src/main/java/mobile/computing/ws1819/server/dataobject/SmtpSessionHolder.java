package mobile.computing.ws1819.server.dataobject;

import mobile.computing.ws1819.shared.model.Session;

import java.util.concurrent.ConcurrentHashMap;

/*
this class is just a holder class for user's session object
*/
public class SmtpSessionHolder {
	public static ConcurrentHashMap<String, Session> userSession = new ConcurrentHashMap<>();
}
