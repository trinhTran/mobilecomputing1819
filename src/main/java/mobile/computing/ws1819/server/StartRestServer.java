package mobile.computing.ws1819.server;

import java.io.IOException;

import com.sun.jersey.api.container.httpserver.HttpServerFactory;
import com.sun.net.httpserver.HttpServer;
import mobile.computing.ws1819.shared.utils.ConsoleInterpreter;
import mobile.computing.ws1819.shared.log.LogEvent;
import mobile.computing.ws1819.shared.log.Logger;
import mobile.computing.ws1819.server.dataobject.MailBox;
import mobile.computing.ws1819.shared.constant.SharedConst;
import mobile.computing.ws1819.shared.constant.ServerLimits;
import mobile.computing.ws1819.shared.type.SmtpCommand;

import static mobile.computing.ws1819.shared.constant.SharedConst.SMTP_PORT;

public class StartRestServer
{
	private static final Logger LOGGER = Logger.getLogger(StartRestServer.class);
	private static HttpServer server;

	/*
	now start the SMTP server
	*/
	public static void startServer() {
		try {
			server = HttpServerFactory.create("http://localhost:" + SMTP_PORT + "/");
			server.start();
			LOGGER.info(LogEvent.create("startServer", "REST HTTP Server is started at "+server.getAddress()));
		} catch (IOException e) {
			LOGGER.error(LogEvent.create("startServer", "Cannot start REST HTTP Server due to").add(e));
		}
	}

	/*
	main() method serve as entry point of the application
	 */
	public static void main(String[] args) throws IllegalArgumentException, IOException {
		//now we will reading configurations and ask user to config the server
		initialize();

		//If the server is stop suddenly, we might want to clean up something first
		//A shutdown hook to call server.stop with delay 1 second will be called.
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				server.stop(1);
			}
		});

		//now start the server
		startServer();
	}

	/*
	promt the user the current server config and ask them if they want to reconfigure.
	*/
	public static void initialize() throws IOException {
		MailBox.initialize();// reads mailbox and mailing list configurations
		System.out.println("\033[36m****************************************************\033[0m");
		System.out.println("\033[36m             CONFIGURING SMTP SERVER\033[0m");
		System.out.println("\033[36m****************************************************\033[0m");
		printDefaultSettings(); //print default setting for user to check

		//now we will ask if user want to change the default config or not
		String customizeSetting = ConsoleInterpreter.getAnswerForYesNoQuestion("Do you want to customize settings (yes/no)?");
		if ("yes".equals(customizeSetting.trim())) {
			configurePort();
			configureCommand();
			configureRecipientListSize();
			configureContentSize();
		}
	}
	
	public static void printDefaultSettings() {
		System.out.println("");
		System.out.println("\033[36m>>>>>>> DEFAULT SETTINGS\033[0m");
		System.out.println("");
		System.out.println("\033[36m*-- Port           : "+ SharedConst.SMTP_PORT + "\033[0m");
		System.out.println("\033[36m*-- SMTP Context   : "+ SharedConst.SMTP_CONTEXT + "\033[0m");
		System.out.println("\033[36m*-- Enabled command: "+ SmtpCommand.getEnabledCommandList().toString() + "\033[0m");
		System.out.println("");
		System.out.println("\033[36m*-- Max RECIPIENT_LIST_SIZE        : "+ ServerLimits.RECIPIENT_LIST_SIZE.getUpperBound().toString() + "\033[0m");
		System.out.println("\033[36m*-- Max MESSAGE_CONTENT_SIZE (byte): "+ ServerLimits.MESSAGE_CONTENT_SIZE.getUpperBound().toString() + "\033[0m");
	}
	
	public static void configurePort() {
		int port = -1;
		//will break only when user enter a valid port number
		while (port < 0) {
			String portStr = ConsoleInterpreter.getAnswerForNormalQuestion("Which port do you want to used?");
			try {
				port = Integer.parseInt(portStr);
				if (port <= 0 || port >= 65535) {
					port = -1;
					System.out.println("\033[31mERROR: Invalid port nummer. Port must be between 1 and 65535.\033[0m");
				}
			} catch (Exception e) {
				System.out.println("\033[31mERROR: Invalid format. Port must be an interger number\033[0m");
			}
		}
		SharedConst.SMTP_PORT = String.valueOf(port);
	}
	
	public static void configureCommand() {
		String enabledHelpCommand = ConsoleInterpreter.getAnswerForYesNoQuestion("Do you want to enable HELP command (yes/no)?");
		if ("yes".equals(enabledHelpCommand)) {
			SmtpCommand.HELP.enabled = true;
		} else {
			SmtpCommand.HELP.enabled = false;
		}
		
		String enabledVrfyCommand = ConsoleInterpreter.getAnswerForYesNoQuestion("Do you want to enable VRFY command (yes/no)?");
		if ("yes".equals(enabledVrfyCommand)) {
			SmtpCommand.VRFY.enabled = true;
		} else {
			SmtpCommand.VRFY.enabled = false;
		}
		
		String enabledExpnCommand = ConsoleInterpreter.getAnswerForYesNoQuestion("Do you want to enable EXPN command (yes/no)?");
		if ("yes".equals(enabledExpnCommand)) {
			SmtpCommand.EXPN.enabled = true;
		} else {
			SmtpCommand.EXPN.enabled = false;
		}
	}
	
	public static void configureRecipientListSize() {
		int size = -1;
		while (size < 0) {
			String recipientListSize = ConsoleInterpreter.getAnswerForNormalQuestion("Please enter wished size for recipient list?");
			try {
				size = Integer.parseInt(recipientListSize);
			} catch (Exception e) {
				System.out.println("\033[31mERROR: Invalid format. Size must be an interger number\033[0m");
			}
		}
		ServerLimits.RECIPIENT_LIST_SIZE.setUpperBound(size);
	}
	
	public static void configureContentSize()  {
		int size = -1;
		while (size < 0) {
			String contentSize = ConsoleInterpreter.getAnswerForNormalQuestion("Please enter wished size for email content (Byte)?");
			try {
				size = Integer.parseInt(contentSize);
			} catch (Exception e) {
				System.out.println("\033[31mERROR: Invalid format. Size must be an interger number\033[0m");
			}
		}
		ServerLimits.MESSAGE_CONTENT_SIZE.setUpperBound(size);
	}
}
