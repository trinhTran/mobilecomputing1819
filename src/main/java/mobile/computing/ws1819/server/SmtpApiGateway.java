package mobile.computing.ws1819.server;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import com.sun.jersey.spi.container.ContainerRequest;
import mobile.computing.ws1819.server.dataobject.SmtpSessionHolder;
import mobile.computing.ws1819.shared.log.LogEvent;
import mobile.computing.ws1819.shared.log.Logger;
import mobile.computing.ws1819.shared.model.Message;
import mobile.computing.ws1819.shared.model.Session;
import mobile.computing.ws1819.shared.constant.SharedConst;
import mobile.computing.ws1819.shared.type.*;
import mobile.computing.ws1819.shared.type.HttpMethod;

/*
This class will function as the single gateway when a request is comming into the server.
This gateway will do some validation on the request and then dispatch that request to 
the corresponding smtp command handler. 
*/
@Path(SharedConst.SMTP_CONTEXT)
public class SmtpApiGateway extends Application {

	private static final Logger LOGGER = Logger.getLogger(SmtpApiGateway.class);

	private Response requestDispatch(Request req, String smtpMethod, String messageAsJsonString) {
		ContainerRequest request = (ContainerRequest) req;
		SmtpCommand smtpCommand = SmtpCommand.getByName(smtpMethod.trim());

		LOGGER.info(LogEvent.create("requestDispatch", "received new request")
				.add("method", request.getMethod())
				.add("path", request.getPath())
				.add("request object", request.toString())
				.add("message", messageAsJsonString));

		/*
		Now we gonna do some validations before dispatching the request to the handler
		First we will check if the SMTP is the command is supported or not.
		If when we try to get an SmtpCommand Enum value from the string and it could not be found
		then the smtpCommand reference will be null
		*/
		if (smtpCommand == null) {
			LOGGER.warn(LogEvent.create("requestDispatch", "the requested smtp command '"+ smtpMethod+ "' cannot been found"));
			Message responseMessage = new Message.MessageBuilder()
					.setHttpStatus(HttpStatus.OK.getStatusCode())
					.setSmtpStatus(SmtpStatus._500.getCode())
					.setTimestamp(String.valueOf(System.currentTimeMillis())).build();
			return Response.status(HttpStatus.OK).entity(responseMessage.toJsonString()).build();
		}

		//check if the HTTP method, that is used to request the smtp command is correct
		if (!smtpCommand.httpMethod.equals(HttpMethod.getByName(request.getMethod()))) {
			LOGGER.warn(LogEvent.create("requestDispatch", "SMTP command '" + smtpMethod + "' dont support HTTP Method '" + request.getMethod() + "'"));
			return Response.status(HttpStatus.METHOD_NOT_ALLOWED).build();
		}

		//we never accept null message
		if (messageAsJsonString == null) {
			LOGGER.warn(LogEvent.create("requestDispatch", "message cannot be null"));
			return Response.status(HttpStatus.BAD_REQUEST).build();
		}
		
		try {
			Message requestMessage = (new Message.MessageBuilder()).newFromJsonString(messageAsJsonString).build();
			
			//PING is a special command, it is used to generate sessions.
			//So if the command is PING, we not gonna check for session
			if (smtpCommand == SmtpCommand.PING) {
				return smtpCommand.getCommandHandler().processCommand(smtpCommand, requestMessage);
			}

			//we will check if the current sessionID is still valid or not.
			//It may be the case that server has been restarted, so when client try to access a session with old
			//session UUID, it could not be found.
			Session session = SmtpSessionHolder.userSession.get(requestMessage.getSmtpSessionUID());
			if (session == null) { 
				// is these is no session associate with the UUID, then there must be something wrong with the client
				LOGGER.warn(LogEvent.create("requestDispatch", "could not find the session for this client"));
				Message responseMessage = new Message.MessageBuilder()
						.setHttpStatus(HttpStatus.OK.getStatusCode())
						.setTimestamp(String.valueOf(System.currentTimeMillis()))
						.setSmtpStatus(SmtpStatus._451.getCode())
						.setSmtpMessageContent(SmtpStatus._451.getStdMessage())
						.setCurrentSmtpStateOrder(SmtpStates.SESSION_NOT_INITIALIZED.getStateOrder()).build();

				return Response.status(HttpStatus.OK).entity(responseMessage.toJsonString()).build();
			}

			SmtpStates currentState = session.getCurrentSmtpState();
			//Then we check if the command is enabled on or not
			if (!smtpCommand.isEnabled()) {
				LOGGER.warn(LogEvent.create("requestDispatch", "the requested smtp command '"+ smtpMethod+ "' is not yet implemented"));
				Message responseMessage = new Message.MessageBuilder()
						.setHttpStatus(HttpStatus.OK.getStatusCode())
						.setSmtpStatus(SmtpStatus._502.getCode())
						.setSmtpMessageContent(SmtpStatus._502.getStdMessage())
						.setTimestamp(String.valueOf(System.currentTimeMillis()))
						.setCurrentSmtpStateOrder(currentState.getStateOrder()).build();
				
				return Response.status(HttpStatus.OK).entity(responseMessage.toJsonString()).build();
			} 

			//Check if the command is send at the correct state
			//Because a command is only allowed at a certain states.
			if (!smtpCommand.isAllowedAtState(currentState)) {
				LOGGER.warn(LogEvent.create("requestDispatch", "the requested smtp command '"+ smtpMethod+ "' is not allowed at this stage").add("command", smtpCommand.name).add("currentState", currentState.name()));
				Message responseMessage = new Message.MessageBuilder()
						.setHttpStatus(HttpStatus.OK.getStatusCode())
						.setSmtpStatus(SmtpStatus._503.getCode())
						.setSmtpMessageContent(SmtpStatus._503.getStdMessage())
						.setTimestamp(String.valueOf(System.currentTimeMillis()))
						.setCurrentSmtpStateOrder(currentState.getStateOrder()).build();

				return Response.status(HttpStatus.OK).entity(responseMessage.toJsonString()).build();
			}
			
			String smtpMessageContent = requestMessage.getSmtpMessageContent();
			//finally we validate the SMTP message syntax
			if (!smtpCommand.isSmtpMessageSyntaxValid(smtpMessageContent)) {
				//we will check it is at DATA command stage 2 when it is waiting for mail body.
				if (smtpCommand != SmtpCommand.DATA || currentState != SmtpStates.RECEIVING_DATA) {
					// Nope! It is not at stage 2 of DATA command
					// This must be a wrong syntax.
					Message responseMessage = new Message.MessageBuilder()
							.setHttpStatus(HttpStatus.OK.getStatusCode())
							.setSmtpStatus(SmtpStatus._500.getCode())
							.setSmtpMessageContent(SmtpStatus._500.getStdMessage())
							.setTimestamp(String.valueOf(System.currentTimeMillis()))
							.setCurrentSmtpStateOrder(currentState.getStateOrder()).build();

					return Response.status(HttpStatus.OK).entity(responseMessage.toJsonString()).build();
				}
			}
			
			//no every validation is done, everything fine, we gonna route it the correct command handler
			//base on smtpCommand type.
			return smtpCommand.getCommandHandler().processCommand(smtpCommand, requestMessage);
		} catch (Exception e) {
			LOGGER.error(LogEvent.create("requestDispatch", "there are errors when trying to process smtp command").add(e));
			return Response.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	@GET
	@Path("{smtpMethod}")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.TEXT_PLAIN)
	public Response smtpMethodRequest_GET(@Context Request req, @PathParam("smtpMethod") String smtpMethod, String messageAsJsonString) {
		return requestDispatch(req, smtpMethod, messageAsJsonString);
	}

	@POST
	@Path("{smtpMethod}")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.TEXT_PLAIN)
	public Response smtpMethodRequest_POST(@Context Request req, @PathParam("smtpMethod") String smtpMethod, String messageAsJsonString) {
		return requestDispatch(req, smtpMethod, messageAsJsonString);
	}

	@PUT
	@Path("{smtpMethod}")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.TEXT_PLAIN)
	public Response smtpMethodRequest_PUT(@Context Request req, @PathParam("smtpMethod") String smtpMethod, String messageAsJsonString) {
		return requestDispatch(req, smtpMethod, messageAsJsonString);
	}

	@DELETE
	@Path("{smtpMethod}")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.TEXT_PLAIN)
	public Response smtpMethodRequest_DELETE(@Context Request req, @PathParam("smtpMethod") String smtpMethod, String messageAsJsonString) {
		return requestDispatch(req, smtpMethod, messageAsJsonString);
	}
}
