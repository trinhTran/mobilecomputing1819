package mobile.computing.ws1819.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import mobile.computing.ws1819.shared.log.LogEvent;
import mobile.computing.ws1819.shared.log.Logger;
import mobile.computing.ws1819.shared.model.Message;
import mobile.computing.ws1819.shared.constant.SharedConst;
import mobile.computing.ws1819.shared.type.SmtpCommand;

import javax.ws.rs.core.MediaType;
import java.io.IOException;

public class SmtpApiConnector implements ApiConnector{
	private static final Logger LOGGER = Logger.getLogger(StartRestClient.class);
	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	private String smtpRestServerUrl;
	private Client restClient;
	private WebResource restServiceResource;

	public SmtpApiConnector(String smtpRestServerUrl) {
		restClient = Client.create();
		this.smtpRestServerUrl = smtpRestServerUrl;
		restServiceResource = restClient.resource(this.smtpRestServerUrl);
	}

	public SmtpApiConnector(String smtpServerDomain, String smtpServerPort) {
		this("http://" + smtpServerDomain + ":" + smtpServerPort + SharedConst.SMTP_CONTEXT);
	}


	/*
	Any thing that needed to send a request to the server will call through this method.
	This method will decide which HTTP method to use
 	*/
	@Override
	public Message request(Message message, SmtpCommand smtpCommand) {
		LOGGER.debug(LogEvent.create("request", "selecting correct HTTP method for requested smtp command")
				.add("message", message.toString())
				.add("smtpCommand", smtpCommand.getName()));

		String messageAsJsonString = message.toJsonString();
		try {
			switch (smtpCommand.httpMethod) {
				case GET:
					return doGetRequest(messageAsJsonString, smtpCommand);
				case PUT:
					return doPutRequest(messageAsJsonString, smtpCommand);
				case POST:
					return doPostRequest(messageAsJsonString, smtpCommand);
				case DELETE:
					return doDeleteRequest(messageAsJsonString, smtpCommand);
			}
		} catch (Exception e) {
			LOGGER.error(LogEvent.create("request", "there are error when trying to request SMTP api")
					.add("message", messageAsJsonString)
					.add("smtpCommand", smtpCommand.getName())
					.add(e));
			throw new RuntimeException(e);
		}
		return null;
	}

	@Override
	public Message doGetRequest(String message, SmtpCommand smtpCommand) throws IOException
	{
		LOGGER.debug(LogEvent.create("doGetRequest", "send a GET request to smtp api")
				.add("smtpCommand", smtpCommand.getName()));

		// Send GET request#
		String apiPath = smtpCommand.getName();
		String response = restServiceResource.path(apiPath).accept(MediaType.TEXT_PLAIN).get(String.class);
		LOGGER.debug(LogEvent.create("doGetRequest", "received back a response in JSON")
			.add("response", response));

		return OBJECT_MAPPER.readValue(response, Message.class);
	}

	@Override
	public Message doPostRequest(String message, SmtpCommand smtpCommand) throws IOException
	{
		LOGGER.debug(LogEvent.create("doPostRequest", "send a POST request to smtp api")
				.add("message", message)
				.add("smtpCommand", smtpCommand.getName()));

		// Send POST request
		String apiPath = smtpCommand.getName();
		String response = restServiceResource.path(apiPath).type(MediaType.TEXT_PLAIN).post(String.class, message);
		LOGGER.debug(LogEvent.create("doPostRequest", "received back a response in JSON")
				.add("response", response));

		return OBJECT_MAPPER.readValue(response, Message.class);
	}

	@Override
	public Message doPutRequest(String message, SmtpCommand smtpCommand) throws IOException
	{
		LOGGER.debug(LogEvent.create("doPutRequest", "send a PUT request to smtp api")
				.add("message", message)
				.add("smtpCommand", smtpCommand.getName()));

		// Send POST request
		String apiPath = smtpCommand.getName();
		String response = restServiceResource.path(apiPath).type(MediaType.TEXT_PLAIN).put(String.class, message);
		LOGGER.debug(LogEvent.create("doPutRequest", "received back a response in JSON")
				.add("response", response));

		return OBJECT_MAPPER.readValue(response, Message.class);
	}

	@Override
	public Message doDeleteRequest(String message, SmtpCommand smtpCommand) throws IOException
	{
		LOGGER.debug(LogEvent.create("doDeleteRequest", "send a DELETE request to smtp api")
				.add("message", message)
				.add("smtpCommand", smtpCommand.getName()));

		// Send POST request
		String apiPath = smtpCommand.getName();
		String response = restServiceResource.path(apiPath).type(MediaType.TEXT_PLAIN).delete(String.class, message);
		LOGGER.debug(LogEvent.create("doDeleteRequest", "received back a response in JSON")
				.add("response", response));

		return OBJECT_MAPPER.readValue(response, Message.class);
	}

	@Override
	public String toString() {
		return "[This connector will connect with http api at URL: "+ this.smtpRestServerUrl+ " ]";
	}
}
