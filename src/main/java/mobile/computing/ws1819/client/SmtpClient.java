package mobile.computing.ws1819.client;

import mobile.computing.ws1819.shared.model.Session;
import mobile.computing.ws1819.shared.type.SmtpCommand;

public interface SmtpClient {

	//Setting up necessary configurations in order to connect to SMTP server and start SMTP client
	void initialize();

	//Start the smtp client try to connect to smtp server and startServer and SMTP session
	void start();

	//This method will send PING message to SMTP server to create a new SMTP session for the current user
	Session createNewSession() throws Exception;

	//This method will start the main application loop and ask the user for their wished of SMTP command
	void startSmtpControlPanel() throws Exception;

	//Each one of these method will responsible for interacting with user to get the data
	//then constructing request Message object, give it to ApiConnector.request(), and waiting for
	//response Message object to be returned.
	//When receiving a response Message object, the current state which is sent from server will be
	//updated into own session object.
	void doHeloCommand(SmtpCommand command) throws Exception;
	void doEhloCommand(SmtpCommand command) throws Exception;
	void doMailCommand(SmtpCommand command) throws Exception;
	void doRcptCommand(SmtpCommand command) throws Exception;
	void doDataCommand(SmtpCommand command) throws Exception;
	void doRsetCommand(SmtpCommand command) throws Exception;
	void doQuitCommand(SmtpCommand command) throws Exception;
	void doHelpCommand(SmtpCommand command) throws Exception;
	void doVrfyCommand(SmtpCommand command) throws Exception;
	void doExpnCommand(SmtpCommand command) throws Exception;
	void doNoopCommand(SmtpCommand command) throws Exception;
}
