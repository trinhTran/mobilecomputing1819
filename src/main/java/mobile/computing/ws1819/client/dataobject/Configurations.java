package mobile.computing.ws1819.client.dataobject;

/*
When client start it need to known the domain and port of the client.
So it will ask user to provide it.
These configuration will be stored here
*/
public class Configurations {
	public static String SMTP_REST_SERVER_DOMAIN;
	public static String SMTP_SERVER_PORT;
}
