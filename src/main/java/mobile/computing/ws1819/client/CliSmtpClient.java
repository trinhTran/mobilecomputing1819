package mobile.computing.ws1819.client;

import static mobile.computing.ws1819.shared.constant.SharedConst.CRLF;
import static mobile.computing.ws1819.shared.constant.SharedConst.SP;

import java.net.InetAddress;
import java.util.Arrays;

import mobile.computing.ws1819.shared.utils.ConsoleInterpreter;
import mobile.computing.ws1819.client.dataobject.Configurations;
import mobile.computing.ws1819.shared.log.LogEvent;
import mobile.computing.ws1819.shared.log.Logger;
import mobile.computing.ws1819.shared.model.Message;
import mobile.computing.ws1819.shared.model.Session;
import mobile.computing.ws1819.shared.type.*;
import mobile.computing.ws1819.shared.utils.CrossPlatformFormater;

public class CliSmtpClient implements SmtpClient {
	private static final Logger LOGGER = Logger.getLogger(CliSmtpClient.class);

	private ApiConnector connector;
	private Session session;

	@Override
	public void initialize() {
		configureSmtpServer();
	}

	/*
	Ask user to enter domain of the server and the port
	 */
	private void configureSmtpServer() {
		System.out.println("\033[36m****************************************************\033[0m");
		System.out.println("\033[36m             CONFIGURING SMTP CONNECTOR\033[0m");
		System.out.println("\033[36m****************************************************\033[0m");
		Configurations.SMTP_REST_SERVER_DOMAIN = ConsoleInterpreter.getAnswerForNormalQuestion("What is SMTP server domain?");
		Configurations.SMTP_SERVER_PORT = ConsoleInterpreter.getAnswerForNormalQuestion("What is SMTP server port?");
		connector = new SmtpApiConnector(Configurations.SMTP_REST_SERVER_DOMAIN,
										Configurations.SMTP_SERVER_PORT);

		System.out.println("\033[36m\n|~~> " + connector.toString()+ "\033[0m");
	}

	@Override
	public void start() {
		try {
			this.session = createNewSession();
			startSmtpControlPanel();
		} catch (Exception e) {
			LOGGER.error(LogEvent.create("start", "SMTP client is no longer working").add(e));
		}
	}

	/*
	REST is stateless, so in order to simulate a SMTP state-full connection, we
	will send PING command (non standard SMTP command) to get and session UUID.
	Using this session UUID, we will be able to let the SMTP server known our session states table. 
	*/
	@Override
	public Session createNewSession() throws Exception {
		LOGGER.debug(LogEvent.create("createNewSession", "trying to intialize connection and a new SMTP session"));
		Session newSession = null;
		Message initialMessage = new Message();
		initialMessage.setCurrentSmtpStateOrder(SmtpStates.SESSION_NOT_INITIALIZED.getStateOrder());
		initialMessage.setTimestamp(String.valueOf(System.currentTimeMillis()));
		initialMessage.setSmtpMessageContent("Initialize connection and session");

		Message responseMessage = connector.request(initialMessage, SmtpCommand.PING);
		if (responseMessage != null) {
			newSession = new Session(); //We will also create a local session object to keep track on the session.
			newSession.setUuid(responseMessage.getSmtpSessionUID());
			//update state,which is sent from server, into its own session object
			newSession.setCurrentSmtpState(SmtpStates.getByOrder(responseMessage.getCurrentSmtpStateOrder()));
			LOGGER.debug(LogEvent.create("createNewSession", "a new SMTP session is successfully created"));
		} else {
			throw new RuntimeException("cannot startServer a new session due to unknown error");
		}
		return newSession;
	}

	/*
	This contains the main loop of the client application, where the command choosing happen.
	*/
	@Override
	public void startSmtpControlPanel() throws Exception {
		/*
		After sending QUIT command, server will send back a message with state SESSION_NOT_INITIALIZED,
		client will update that state into its session object. Then the loop below will break.
		 */
		while (session.getCurrentSmtpState() != SmtpStates.SESSION_NOT_INITIALIZED) {
			StringBuilder smtpCommandQuestion = new StringBuilder();
			smtpCommandQuestion.append("Please enter the name of an SMTP command to proceed: ").append(System.lineSeparator());
			smtpCommandQuestion.append("-> : ").append(Arrays.asList(SmtpCommand.getAllStandardSmtpCommand()).toString());
			String questionAnswer = ConsoleInterpreter.getAnswerForNormalQuestion(smtpCommandQuestion.toString());
			SmtpCommand choosenSmtpCommand =  SmtpCommand.getByName(questionAnswer.toUpperCase());

			if (choosenSmtpCommand == null) {
				System.out.println("This " + questionAnswer + " command is not a valid SMTP command. Please choose again.\n");
				continue;
			}

			switch (choosenSmtpCommand) {
				case HELO:
					doHeloCommand(choosenSmtpCommand);
					break;
				case EHLO:
					doEhloCommand(choosenSmtpCommand);
					break;
				case MAIL:
					doMailCommand(choosenSmtpCommand);
					break;
				case RCPT:
					doRcptCommand(choosenSmtpCommand);
					break;
				case DATA:
					doDataCommand(choosenSmtpCommand);
					break;
				case RSET:
					doRsetCommand(choosenSmtpCommand);
					break;
				case QUIT:
					doQuitCommand(choosenSmtpCommand);
					break;
				case HELP:
					doHelpCommand(choosenSmtpCommand);
					break;
				case VRFY:
					doVrfyCommand(choosenSmtpCommand);
					break;
				case EXPN:
					doExpnCommand(choosenSmtpCommand);
					break;
				case NOOP:
					doNoopCommand(choosenSmtpCommand);
					break;
				default:
					System.out.println("This " + questionAnswer + " command is not a valid SMTP command. Please choose again.\n");
					break;
			}
		}
	}

	@Override
	public void doHeloCommand(SmtpCommand command) throws Exception {
		//constructing HELO command
		String domain = ConsoleInterpreter.getAnswerForNormalQuestion("Please enter your domain?");
		String smtpContent = "HELO"+SP+domain+CRLF;
		Message HELOMessage = getNewMessageWithContent(smtpContent);
		
		//send and handle HELO command response
		Message responeMessage = connector.request(HELOMessage, command);
		if (responeMessage != null) {
			SmtpStatus responseSmtpStatus = SmtpStatus.getByCode(responeMessage.getSmtpStatus());
			if (responseSmtpStatus.getType() == StatusType.SUCCESS) {
				System.out.println("SUCCESS: HELO command was sent successfully: " 
						+ System.lineSeparator()
						+ CrossPlatformFormater.changeToPlatformSpecificEOL(responeMessage.getSmtpMessageContent()));
				session.setClientDomain(domain);
			} else {
				System.out.println("ERROR: error has occured when trying to sent HELO command: " 
						+ System.lineSeparator()
						+ CrossPlatformFormater.changeToPlatformSpecificEOL(responeMessage.getSmtpMessageContent()));
			}
			//update state,which is sent from server, into its own session object
			session.setCurrentSmtpState(SmtpStates.getByOrder(responeMessage.getCurrentSmtpStateOrder()));
		}
	}

	@Override
	public void doEhloCommand(SmtpCommand command) throws Exception {
		//constructing EHLO command
		String domain = ConsoleInterpreter.getAnswerForNormalQuestion("Please enter your domain or you can leave it blank and I'll take your IP?");
		if (domain == null || domain.isEmpty()) {
			domain = InetAddress.getLocalHost().toString();
		}
		String smtpContent = "EHLO"+SP+domain+CRLF;
		Message EHLOMessage = getNewMessageWithContent(smtpContent);

		//send and handle EHLO command response
		Message responeMessage = connector.request(EHLOMessage, command);
		if (responeMessage != null) {
			SmtpStatus responseSmtpStatus = SmtpStatus.getByCode(responeMessage.getSmtpStatus());
			if (responseSmtpStatus.getType() == StatusType.SUCCESS) {
				System.out.println("SUCCESS: EHLO command was sent successfully: " 
						+ System.lineSeparator()
						+ CrossPlatformFormater.changeToPlatformSpecificEOL(responeMessage.getSmtpMessageContent()));
				session.setClientDomain(domain);
			} else {
				System.out.println("ERROR: error has occured when trying to sent EHLO command: " 
						+ System.lineSeparator()
						+ CrossPlatformFormater.changeToPlatformSpecificEOL(responeMessage.getSmtpMessageContent()));
			}
			//update state,which is sent from server, into its own session object
			session.setCurrentSmtpState(SmtpStates.getByOrder(responeMessage.getCurrentSmtpStateOrder()));
		}
	}

	@Override
	public void doMailCommand(SmtpCommand command) throws Exception {
		//constructing MAIL command
		String senderMailBox = ConsoleInterpreter.getAnswerForNormalQuestion("Please enter the your mailbox address?");
		String smtpContent = "MAIL FROM:<"+senderMailBox+">"+CRLF;
		Message MAILMessage = getNewMessageWithContent(smtpContent);

		//send and handle MAIL command response
		Message responeMessage = connector.request(MAILMessage, command);
		if (responeMessage != null) {
			SmtpStatus responseSmtpStatus = SmtpStatus.getByCode(responeMessage.getSmtpStatus());
			if (responseSmtpStatus.getType() == StatusType.SUCCESS) {
				System.out.println("SUCCESS: MAIL command was sent successfully: " 
						+ System.lineSeparator()
						+ CrossPlatformFormater.changeToPlatformSpecificEOL(responeMessage.getSmtpMessageContent()));
				session.setReverseMailBox(senderMailBox);
			} else {
				System.out.println("ERROR: error has occured when trying to sent MAIL command: " 
						+ System.lineSeparator()
						+ CrossPlatformFormater.changeToPlatformSpecificEOL(responeMessage.getSmtpMessageContent()));
			}
			//update state,which is sent from server, into its own session object
			session.setCurrentSmtpState(SmtpStates.getByOrder(responeMessage.getCurrentSmtpStateOrder()));

		}
	}

	@Override
	public void doRcptCommand(SmtpCommand command) throws Exception {
		//constructing RCPT command
		String recipientMailBox = ConsoleInterpreter.getAnswerForNormalQuestion("Please enter the recipient mailbox address?");
		String smtpContent = "RCPT TO:<"+recipientMailBox+">"+CRLF;
		Message RCPTMessage = getNewMessageWithContent(smtpContent);

		//send and handle RCPT command response
		Message responeMessage = connector.request(RCPTMessage, command);
		if (responeMessage != null) {
			SmtpStatus responseSmtpStatus = SmtpStatus.getByCode(responeMessage.getSmtpStatus());
			if (responseSmtpStatus.getType() == StatusType.SUCCESS) {
				System.out.println("SUCCESS: RCPT command was sent successfully: " 
						+ System.lineSeparator()
						+ CrossPlatformFormater.changeToPlatformSpecificEOL(responeMessage.getSmtpMessageContent()));
				session.tryToAddNewRecipient(recipientMailBox);
			} else {
				System.out.println("ERROR: error has occured when trying to sent RCPT command: " 
						+ System.lineSeparator()
						+ CrossPlatformFormater.changeToPlatformSpecificEOL(responeMessage.getSmtpMessageContent()));
			}
			//update state,which is sent from server, into its own session object
			session.setCurrentSmtpState(SmtpStates.getByOrder(responeMessage.getCurrentSmtpStateOrder()));

		}
	}

	@Override
	public void doDataCommand(SmtpCommand command) throws Exception {
		//step 1: send DATA command to inform server that we are going to send mail data
		//constructing DATA command
		String smtpContentStep1 = "DATA"+CRLF;
		Message DATAMessageStep1 = getNewMessageWithContent(smtpContentStep1);
		Message responeMessageStep1 = connector.request(DATAMessageStep1, command);
		if (responeMessageStep1 != null) {
			SmtpStatus responseSmtpStatus = SmtpStatus.getByCode(responeMessageStep1.getSmtpStatus());
			if ((responseSmtpStatus.getType() == StatusType.SUCCESS) && (responseSmtpStatus == SmtpStatus._354)) {
				System.out.println("SUCCESS: DATA command was sent successfully: " 
						+ System.lineSeparator()
						+ CrossPlatformFormater.changeToPlatformSpecificEOL(responeMessageStep1.getSmtpMessageContent()));
			} else {
				System.out.println("ERROR: error has occured when trying to sent DATA command: " 
						+ System.lineSeparator()
						+ CrossPlatformFormater.changeToPlatformSpecificEOL(responeMessageStep1.getSmtpMessageContent()));
			}
			//update state,which is sent from server, into its own session object
			session.setCurrentSmtpState(SmtpStates.getByOrder(responeMessageStep1.getCurrentSmtpStateOrder()));
		} else {
			throw new RuntimeException("Response message from server to DATA command is null");
		}

		//step 2: asking user to enter data to be sent. Then send data to server
		String accumulatedData = "";
		String fromLine = ConsoleInterpreter.getAnswerForNormalQuestion("Please enter From line (or just leave it blank)?");
		if (fromLine != null && !fromLine.isEmpty()) {
			if (!fromLine.startsWith("From") && !fromLine.contains("from") && !fromLine.startsWith("FROM")) {
				if (!fromLine.contains(":")) {
					fromLine = ":" + fromLine;
				}
				fromLine = "From" + fromLine;
			}
			fromLine += CRLF;
			accumulatedData += fromLine;
		}
		String toLine = ConsoleInterpreter.getAnswerForNormalQuestion("Please enter To line (or just leave it blank)?");
		if (toLine != null && !toLine.isEmpty()) {
			if (!toLine.startsWith("To") && !toLine.contains("to") && !toLine.startsWith("TO")) {
				if (!toLine.contains(":")) {
					toLine = ":" + toLine;
				}
				toLine = "To" + toLine;
			}
			toLine += CRLF;
			accumulatedData += toLine;
		}
		String subjectLine = ConsoleInterpreter.getAnswerForNormalQuestion("Please enter Subject line (or just leave it blank)?");
		if (subjectLine != null && !subjectLine.isEmpty()) {
			if (!subjectLine.startsWith("Subject") && !subjectLine.contains("subject") && !subjectLine.startsWith("SUBJECT")) {
				if (!subjectLine.contains(":")) {
					subjectLine = ":" + subjectLine;
				}
				subjectLine = "Subject" + subjectLine;
			}
			subjectLine += CRLF;
			accumulatedData += subjectLine;
		}

		/*
		We will read until we hit <CRLF>.<CRLF> at end
		*/
		System.out.println("\033[35m--> Now type input data to be sent. Ending with <CRLF>.<CRLF>\033[0m");
		while (!accumulatedData.endsWith(CRLF+"."+CRLF)) {
			String line = ConsoleInterpreter.getInputByLine() + CRLF;
			accumulatedData += line;
		}

		Message DATAMessage2 = getNewMessageWithContent(accumulatedData);
		Message responeMessageStep2 = connector.request(DATAMessage2, command);
		if (responeMessageStep2 != null) {
			SmtpStatus responseSmtpStatus = SmtpStatus.getByCode(responeMessageStep2.getSmtpStatus());
			if (responseSmtpStatus.getType() == StatusType.SUCCESS) {
				System.out.println("SUCCESS: DATA command was sent successfully: " 
						+ System.lineSeparator()
						+ CrossPlatformFormater.changeToPlatformSpecificEOL(responeMessageStep2.getSmtpMessageContent()));
			} else {
				System.out.println("ERROR: error has occured when trying to sent DATA command: " 
						+ System.lineSeparator()
						+ CrossPlatformFormater.changeToPlatformSpecificEOL(responeMessageStep2.getSmtpMessageContent()));
			}
			//update state,which is sent from server, into its own session object
			session.setCurrentSmtpState(SmtpStates.getByOrder(responeMessageStep2.getCurrentSmtpStateOrder()));
		}
	}

	@Override
	public void doRsetCommand(SmtpCommand command) throws Exception {
		//constructing RSET command
		String smtpContent = "RSET"+CRLF;
		Message RSETMessage = getNewMessageWithContent(smtpContent);

		//send and handle RSET command response
		Message responeMessage = connector.request(RSETMessage, command);
		if (responeMessage != null) {
			SmtpStatus responseSmtpStatus = SmtpStatus.getByCode(responeMessage.getSmtpStatus());
			if (responseSmtpStatus.getType() == StatusType.SUCCESS) {
				System.out.println("SUCCESS: RSET command was sent successfully: " 
						+ System.lineSeparator()
						+ CrossPlatformFormater.changeToPlatformSpecificEOL(responeMessage.getSmtpMessageContent()));
			} else {
				System.out.println("ERROR: error has occured when trying to sent RSET command: " 
						+ System.lineSeparator()
						+ CrossPlatformFormater.changeToPlatformSpecificEOL(responeMessage.getSmtpMessageContent()));
			}
			//update state,which is sent from server, into its own session object
			session.setCurrentSmtpState(SmtpStates.getByOrder(responeMessage.getCurrentSmtpStateOrder()));
		}
	}

	@Override
	public void doQuitCommand(SmtpCommand command) throws Exception {
		//constructing QUIT command
		String smtpContent = "QUIT"+CRLF;
		Message QUITMessage = getNewMessageWithContent(smtpContent);

		//send and handle QUIT command response
		Message responeMessage = connector.request(QUITMessage, command);
		if (responeMessage != null) {
			SmtpStatus responseSmtpStatus = SmtpStatus.getByCode(responeMessage.getSmtpStatus());
			if (responseSmtpStatus.getType() == StatusType.SUCCESS) {
				System.out.println("SUCCESS: QUIT command was sent successfully: " 
						+ System.lineSeparator() 
						+ CrossPlatformFormater.changeToPlatformSpecificEOL(responeMessage.getSmtpMessageContent()));
			} else {
				System.out.println("ERROR: error has occured when trying to sent QUIT command: " 
						+ System.lineSeparator() 
						+ CrossPlatformFormater.changeToPlatformSpecificEOL(responeMessage.getSmtpMessageContent()));
			}
			//update state,which is sent from server, into its own session object
			session.setCurrentSmtpState(SmtpStates.getByOrder(responeMessage.getCurrentSmtpStateOrder()));
		}
	}

	@Override
	public void doHelpCommand(SmtpCommand command) throws Exception {
		//constructing HELP command
		String commandNeedHelp = ConsoleInterpreter.getAnswerForNormalQuestion("Please enter command name (or leave blank to get an overview)?");
		String smtpContent = "HELP";
		if (commandNeedHelp != null && !commandNeedHelp.isEmpty()) {
			smtpContent = smtpContent + SP + commandNeedHelp;
		}
		smtpContent = smtpContent + CRLF;
		Message HELPMessage = getNewMessageWithContent(smtpContent);

		//send and handle HELP command response
		Message responeMessage = connector.request(HELPMessage, command);
		if (responeMessage != null) {
			SmtpStatus responseSmtpStatus = SmtpStatus.getByCode(responeMessage.getSmtpStatus());
			if (responseSmtpStatus.getType() == StatusType.SUCCESS) {
				System.out.println("SUCCESS: HELP command was sent successfully: " 
						+ System.lineSeparator()
						+ CrossPlatformFormater.changeToPlatformSpecificEOL(responeMessage.getSmtpMessageContent()));
			} else {
				System.out.println("ERROR: error has occured when trying to sent HELP command: " 
						+ System.lineSeparator()
						+ CrossPlatformFormater.changeToPlatformSpecificEOL(responeMessage.getSmtpMessageContent()));
			}
			//update state,which is sent from server, into its own session object
			session.setCurrentSmtpState(SmtpStates.getByOrder(responeMessage.getCurrentSmtpStateOrder()));
		}
	}

	@Override
	public void doVrfyCommand(SmtpCommand command) throws Exception {
		//constructing VRFY command
		String domain = ConsoleInterpreter.getAnswerForNormalQuestion("Please enter user name or mailbox address?");
		String smtpContent = "VRFY "+domain+CRLF;
		Message VRFYMessage = getNewMessageWithContent(smtpContent);

		//send and handle VRFY command response
		Message responeMessage = connector.request(VRFYMessage, command);
		if (responeMessage != null) {
			SmtpStatus responseSmtpStatus = SmtpStatus.getByCode(responeMessage.getSmtpStatus());
			if (responseSmtpStatus.getType() == StatusType.SUCCESS) {
				System.out.println("SUCCESS: VRFY command was sent successfully: " 
						+ System.lineSeparator()
						+ CrossPlatformFormater.changeToPlatformSpecificEOL(responeMessage.getSmtpMessageContent()));
			} else {
				System.out.println("ERROR: error has occured when trying to sent VRFY command: " 
						+ System.lineSeparator()
						+ CrossPlatformFormater.changeToPlatformSpecificEOL(responeMessage.getSmtpMessageContent()));
			}
			//update state,which is sent from server, into its own session object
			session.setCurrentSmtpState(SmtpStates.getByOrder(responeMessage.getCurrentSmtpStateOrder()));
		}
	}

	@Override
	public void doExpnCommand(SmtpCommand command) throws Exception {
		//constructing EXPN command
		String mailingList = ConsoleInterpreter.getAnswerForNormalQuestion("Please enter mailing list?");
		String smtpContent = "EXPN "+mailingList+CRLF;
		Message EXPNMessage = getNewMessageWithContent(smtpContent);

		//send and handle EXPN command response
		Message responeMessage = connector.request(EXPNMessage, command);
		if (responeMessage != null) {
			SmtpStatus responseSmtpStatus = SmtpStatus.getByCode(responeMessage.getSmtpStatus());
			if (responseSmtpStatus.getType() == StatusType.SUCCESS) {
				System.out.println("SUCCESS: EXPN command was sent successfully: " 
						+ System.lineSeparator()
						+ CrossPlatformFormater.changeToPlatformSpecificEOL(responeMessage.getSmtpMessageContent()));
			} else {
				System.out.println("ERROR: error has occured when trying to sent EXPN command: " 
						+ System.lineSeparator()
						+ CrossPlatformFormater.changeToPlatformSpecificEOL(responeMessage.getSmtpMessageContent()));
			}
			//update state,which is sent from server, into its own session object
			session.setCurrentSmtpState(SmtpStates.getByOrder(responeMessage.getCurrentSmtpStateOrder()));
		}
	}

	@Override
	public void doNoopCommand(SmtpCommand command) throws Exception {
		//constructing NOOP command
		String smtpContent = "NOOP"+CRLF;
		Message NOOPMessage = getNewMessageWithContent(smtpContent);

		//send and handle NOOP command response
		Message responeMessage = connector.request(NOOPMessage, command);
		if (responeMessage != null) {
			SmtpStatus responseSmtpStatus = SmtpStatus.getByCode(responeMessage.getSmtpStatus());
			if (responseSmtpStatus.getType() == StatusType.SUCCESS) {
				System.out.println("SUCCESS: NOOP command was sent successfully: " 
						+ System.lineSeparator()
						+ CrossPlatformFormater.changeToPlatformSpecificEOL(responeMessage.getSmtpMessageContent()));
			} else {
				System.out.println("ERROR: error has occured when trying to sent NOOP command: " 
						+ System.lineSeparator()
						+ CrossPlatformFormater.changeToPlatformSpecificEOL(responeMessage.getSmtpMessageContent()));
			}
			//update state,which is sent from server, into its own session object
			session.setCurrentSmtpState(SmtpStates.getByOrder(responeMessage.getCurrentSmtpStateOrder()));
		}
	}
	
	public Message getNewMessageWithContent(String messageContent) {
		Message message = new Message();
		message.setCurrentSmtpStateOrder(session.getCurrentSmtpState().getStateOrder());
		message.setSmtpSessionUID(session.getUuid());
		message.setTimestamp(String.valueOf(System.currentTimeMillis()));
		message.setSmtpMessageContent(messageContent);
		return message;
	}
}
