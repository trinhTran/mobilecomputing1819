package mobile.computing.ws1819.client;

import mobile.computing.ws1819.shared.model.Message;
import mobile.computing.ws1819.shared.type.SmtpCommand;

import java.io.IOException;

/*
Classes implement this interface will function as a single end point when an request needed to be send to server,
or when a response coming back from server.
*/
public interface ApiConnector {
	/*
	Base on the SMTP command, this method will use one of those
	do(Get|Post|Put|Delete)Request methods below to sends the REST request to server and get the response back.
	This method serialize the input Message object to send it. When receive back a response it will deserialize
	to a response Message object and return it back.
	*/
	Message request(Message message, SmtpCommand smtpCommand) throws Exception;

	Message doGetRequest(String message, SmtpCommand smtpCommand) throws IOException;
	Message doPostRequest(String message, SmtpCommand smtpCommand) throws IOException;
	Message doPutRequest(String message, SmtpCommand smtpCommand) throws IOException;
	Message doDeleteRequest(String message, SmtpCommand smtpCommand) throws IOException;
}
