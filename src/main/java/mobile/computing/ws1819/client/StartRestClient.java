package mobile.computing.ws1819.client;

/*
* This class serve as the main entry point to client application
*/
public class StartRestClient
{
	private SmtpClient smtpClient;

	public void setSmtpClient(SmtpClient smtpClient) {
		this.smtpClient = smtpClient;
	}

	public void initializeSmtpClient() {
		this.smtpClient.initialize();
	}

	public void startSmtpClient() {
		this.smtpClient.start();
	}


	public static void main(String[] args) {
		StartRestClient restClientIgniter = new StartRestClient();

		/*
		If we start from here we gonna use ManualSmtpClient.
		Some implementation may want to use other implementation of SmtpClient interface.
		 */
		restClientIgniter.setSmtpClient(new CliSmtpClient());

		/*
		First we prompt user to config the Client
		 */
		restClientIgniter.initializeSmtpClient();

		/*
		los geht's
		 */
		restClientIgniter.startSmtpClient();
	}
}
