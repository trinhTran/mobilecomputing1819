package mobile.computing.ws1819.shared.constant;

import mobile.computing.ws1819.shared.type.BoundedValue;

public class ServerLimits {
	/*
	Initialize settings at it minimum default value.
	The unit that is used here is: Octet means 8bits
	*/
	public static BoundedValue.MaxValue<Integer> MESSAGE_CONTENT_SIZE = new BoundedValue.MaxValue<>(64000, 100000);

	/*
	Initialize settings at it minimum value.
	The unit that is used here is per instance.
	*/
	public static BoundedValue.MaxValue<Integer> RECIPIENT_LIST_SIZE = new BoundedValue.MaxValue<>(100, 100);
}
