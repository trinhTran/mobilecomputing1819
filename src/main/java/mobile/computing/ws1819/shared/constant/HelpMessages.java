package mobile.computing.ws1819.shared.constant;

import java.net.InetAddress;
import java.net.UnknownHostException;

import static mobile.computing.ws1819.shared.constant.SharedConst.NEW_LINE;

/*
This class stores String constants.
These are help message, which will be sent to client when they request to HELP command
 */
public class HelpMessages {
	public static String GENERAL = "SMTP SERVER" + NEW_LINE +
			"Version: " + SharedConst.SERVER_VERSION + NEW_LINE +
			"Mics: This SMTP server use REST API." + NEW_LINE;
	static {
		try {
			GENERAL = GENERAL + "IPs: " + InetAddress.getLocalHost().toString()+NEW_LINE;
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		GENERAL = GENERAL + "To see helps for SMTP command please choose help for SMTP command."+NEW_LINE;
	}
	
	public static String PING = 
			"Syntax: PING <CRLF>" + NEW_LINE +
			"Purpose: This is not standard smtp command. It is used to startServer an SMTP sessions." + NEW_LINE;
			
	public static String HELO =
			"Syntax: HELO <SP> Domain <CRLF>" + NEW_LINE +
			"Purpose: The client will introduce itself to SMTP server by using its domain." + NEW_LINE;
	
	public static String EHLO =
			"Syntax: HELO <SP> Domain/Adress literal <CRLF>" + NEW_LINE +
			"Purpose: The client will introduce itself to SMTP server by using its domain or IP address." + NEW_LINE;
	
	public static String MAIL = 
			"Syntax: MAIL FROM:<sender address><CRLF>" + NEW_LINE +
			"Purpose: Telling server that in case delivery failed, bounce the email back to sender address."+ NEW_LINE +
			"         And also startServer a mail transaction."+NEW_LINE;
	
	public static String RCPT = 
			"Syntax: RCPT TO:<recipient address><CRLF>" + NEW_LINE +
			"Purpose: Telling server to add one more recipient for this email." + NEW_LINE;
	
	public static String DATA = 
			"Syntax: DATA<CRLF>" + NEW_LINE +
			"Purpose: Telling server that the client is going to send mail data." + NEW_LINE;
	
	public static String RSET = 
			"Syntax: RSET<CRLF>" + NEW_LINE +
			"Purpose: Cancel mail transaction and reset the state." + NEW_LINE;
	
	public static String QUIT = 
			"Syntax: QUIT<CRLF>" + NEW_LINE +
			"Purpose: Cancel SMTP sessions." + NEW_LINE;
	
	public static String NOOP = 
			"Syntax: NOOP<CRLF>" + NEW_LINE +
			"Purpose: Just hit the server. That's it." + NEW_LINE;
	
	public static String VRFY = 
			"Syntax: VRFY <SP> <String> <CRLF>"+ NEW_LINE +
			"Purpose: Telling server to verify whether <String> is a valid mailbox address or user name." + NEW_LINE;
	
	public static String EXPN = 
			"Syntax: VRFY <SP> <String> <CRLF>"+ NEW_LINE +
			"Purpose: Telling server to send it an list of all mailboxes, that belongs to a mailing list, whose name is match with the <String>." + NEW_LINE;
	
	public static String HELP = 
			"Syntax: HELP[<SP> smtp command]<CRLF>" + NEW_LINE +
			"Purpose: Without smtp command specified, this will show server status and list all supported command."+ NEW_LINE +
			"         With smtp command specified, show the help for that command" + NEW_LINE;
	
}
