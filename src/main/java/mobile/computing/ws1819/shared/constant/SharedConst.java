package mobile.computing.ws1819.shared.constant;

import java.io.File;

/*
This class stored constants that will be used either by server or client or both
 */
public class SharedConst {
	public static final String SERVER_VERSION = "1.0.0-rc1";
	public static final String SMTP_CONTEXT = "/smtp/api";

	public static String SMTP_PORT = "2500";

	//smtp constants
	public static final String SP = " ";
	public static final String CR = "\r";
	public static final String LF = "\n";
	public static final String CRLF = CR+LF;
	public static final String LFCR = LF+CR;

	public static final String NEW_LINE = System.lineSeparator();

	public static final File BASE_DIR = new File(".").getAbsoluteFile();
	public static final File CONF_DIR = new File(BASE_DIR, "configs");
}
