package mobile.computing.ws1819.shared.constant;

/*
Each SMTP command has it own syntax. The pattern of that syntax will be
stored here in String constants, which have more meaningful name
 */
public class SmtpCommandSyntaxPattern {
	public static final String PING_SYNTAX_PATTERN = "[\\s\\w.]*";
	public static final String HELO_SYNTAX_PATTERN = "HELO\\s.*\\s*\\r\\n";
	public static final String EHLO_SYNTAX_PATTERN = "EHLO\\s.*\\s*\\r\\n";
	public static final String MAIL_SYNTAX_PATTERN = "MAIL FROM:<(.*@.*)?>\\s*\\r\\n";
	public static final String RCPT_SYNTAX_PATTERN = "RCPT TO:<.*@.*>\\s*\\r\\n";
	public static final String DATA_SYNTAX_PATTERN = "DATA\\s*\\r\\n";
	public static final String RSET_SYNTAX_PATTERN = "RSET\\s*\\r\\n";
	public static final String NOOP_SYNTAX_PATTERN = "NOOP\\s*.*\\r\\n";
	public static final String QUIT_SYNTAX_PATTERN = "QUIT\\s*\\r\\n";
	public static final String HELP_SYNTAX_PATTERN = "HELP(\\s.*)?\\s*\\r\\n";
	public static final String EXPN_SYNTAX_PATTERN = "EXPN\\s.*\\s*\\r\\n";
	public static final String VRFY_SYNTAX_PATTERN = "VRFY\\s.*\\s*\\r\\n";
}
