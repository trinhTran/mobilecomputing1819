package mobile.computing.ws1819.shared.type;

import static mobile.computing.ws1819.shared.constant.SharedConst.SP;
import static mobile.computing.ws1819.shared.type.StatusType.ERROR;
import static mobile.computing.ws1819.shared.type.StatusType.SUCCESS;

/*
According to RFC 5321-4.2.3
 */
public enum SmtpStatus {
	_211(null, SUCCESS),
	_214(null, SUCCESS),
	_220("SMTP Service ready",  SUCCESS),
	_221("Service closing transmission channel", SUCCESS),
	_250("Requested mail action okay, completed", SUCCESS),
	_251("User not local; will forward to <forward-path>", SUCCESS),
	_252("Cannot VRFY user, but will accept message and attempt delivery", SUCCESS),
	_354("Start mail input; end with <CRLF>.<CRLF>", SUCCESS),
	_421("Service not available, closing transmission channel", ERROR),
	_450("Requested mail action not taken: mailbox unavailable", ERROR),
	_451("Requested action aborted: local error in processing", ERROR),
	_452("Requested action not taken: insufficient system storage", ERROR),
	_455("Server unable to accommodate parameters", ERROR),
	_500("Syntax error, command unrecognized", ERROR),
	_501("Syntax error in parameters or arguments", ERROR),
	_502("Command not implemented", ERROR),
	_503("Bad sequence of commands", ERROR),
	_504("Command parameter not implemented", ERROR),
	_550("Requested action not taken: mailbox unavailable", ERROR),
	_551("User not local; please try another <forward-path>", ERROR),
	_552("Requested mail action aborted: exceeded storage allocation", ERROR),
	_553("Requested action not taken: mailbox name not allowed", ERROR),
	_554("Transaction failed or No SMTP service here", ERROR),
	_555("MAIL FROM/RCPT TO parameters not recognized or not implemented", ERROR);

	private int code;
	private String stdMessage;
	private StatusType type;
	
	SmtpStatus(String stdMessage, StatusType type) {
		this.stdMessage = this.name().replace("_", "") + " " + stdMessage;
		this.code = Integer.valueOf(this.name().replace("_", ""));
		this.type = type;
	}

	public StatusType getType() {
		return this.type;
	}

	public int getCode() {
		return this.code;
	}

	public static SmtpStatus getByCode(int code) {
		for (SmtpStatus status : SmtpStatus.values()) {
			if (status.code == code) {
				return status;
			}
		}
		return null;
	}
	
	public String getStdMessage() {
		return this.stdMessage;
	}
	
	public String toString() {
		return getStdMessage();
	}
	
	public static String generateMessageWithStatus(String stdMessage, SmtpStatus status, boolean isLastLine) {
		StringBuilder bd = new StringBuilder();
		bd.append(String.valueOf(status.getCode()));
		if (isLastLine)
			bd.append(SP);
		else
			bd.append("-");
		bd.append(stdMessage);
		return bd.toString();
	}
}
