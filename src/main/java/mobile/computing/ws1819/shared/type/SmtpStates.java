package mobile.computing.ws1819.shared.type;

public enum SmtpStates {
	SESSION_NOT_INITIALIZED(0),
	SESSION_INITIALIED(1),
	CLIENT_SAID_HELO(2),
	MAIL_TRANSACTION_INITIALIZED(3),
	RECEIVING_RECIPENT(4),
	RECEIVING_DATA(5);

	private int stateOrder;

	SmtpStates(int stateOrder) {
		this.stateOrder = stateOrder;
	}


	public int getStateOrder() {
		return stateOrder;
	}

	public static SmtpStates getByOrder(int order) {
		for (SmtpStates states : SmtpStates.values()) {
			if (states.getStateOrder() == order) {
				return states;
			}
		}
		return null;
	}
}
