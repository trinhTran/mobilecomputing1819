package mobile.computing.ws1819.shared.type;

/*
We will use this to assign to each SMTP command.
Then it could be used to check whether the HTTP method,
that is used to request the SMTP command is allowed or not.
 */
public enum HttpMethod {
	GET("get"),
	POST("post"),
	PUT("put"),
	DELETE("delete");

	public String name;

	HttpMethod(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public static HttpMethod getByName(String name) {
		for (HttpMethod method : HttpMethod.values()) {
			if (method.name.toLowerCase().equals(name.trim().toLowerCase())) {
				return method;
			}
		}
		return null;
	}
}
