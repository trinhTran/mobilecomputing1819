package mobile.computing.ws1819.shared.type;

import static mobile.computing.ws1819.shared.type.StatusType.ERROR;
import static mobile.computing.ws1819.shared.type.StatusType.SUCCESS;

import javax.ws.rs.core.Response;

public enum HttpStatus implements Response.StatusType {
	OK(200, "OK", SUCCESS),
	CREATED(201, "Created", SUCCESS),
	ACCEPTED(202, "Accepted", SUCCESS),
	NO_CONTENT(204, "No Content", SUCCESS),
	MOVED_PERMANENTLY(301, "Moved Permanently", ERROR),
	SEE_OTHER(303, "See Other", ERROR),
	NOT_MODIFIED(304, "Not Modified", ERROR),
	TEMPORARY_REDIRECT(307, "Temporary Redirect", ERROR),
	BAD_REQUEST(400, "Bad Request", ERROR),
	UNAUTHORIZED(401, "Unauthorized", ERROR),
	FORBIDDEN(403, "Forbidden", ERROR),
	NOT_FOUND(404, "Not Found", ERROR),
	METHOD_NOT_ALLOWED(405, "Method is not allowed", ERROR),
	NOT_ACCEPTABLE(406, "Not Acceptable", ERROR),
	CONFLICT(409, "Conflict", ERROR),
	GONE(410, "Gone", ERROR),
	PRECONDITION_FAILED(412, "Precondition Failed", ERROR),
	UNSUPPORTED_MEDIA_TYPE(415, "Unsupported Media Type", ERROR),
	INTERNAL_SERVER_ERROR(500, "Internal Server Error", ERROR),
	SERVICE_UNAVAILABLE(503, "Service Unavailable", ERROR);

	private final int code;
	private final String reason;
	private Response.Status.Family family;
	private StatusType type;

	HttpStatus(int statusCode, String reasonPhrase, StatusType type) {
		this.code = statusCode;
		this.reason = reasonPhrase;
		switch(this.code / 100) {
			case 1:
				this.family = Response.Status.Family.INFORMATIONAL;
				break;
			case 2:
				this.family = Response.Status.Family.SUCCESSFUL;
				break;
			case 3:
				this.family = Response.Status.Family.REDIRECTION;
				break;
			case 4:
				this.family = Response.Status.Family.CLIENT_ERROR;
				break;
			case 5:
				this.family = Response.Status.Family.SERVER_ERROR;
				break;
			default:
				this.family = Response.Status.Family.OTHER;
		}
		this.type = type;

	}

	public Response.Status.Family getFamily() {
		return this.family;
	}

	public int getStatusCode() {
		return this.code;
	}

	public String getReasonPhrase() {
		return this.reason;
	}

	public String toString() {
		return String.valueOf(this.code) + " - " + this.reason;
	}

	public static HttpStatus getByStatusCode(int statusCode) {
		for (HttpStatus status : HttpStatus.values()) {
			if (status.code == statusCode) {
				return status;
			}
		}
		return null;
	}
}
