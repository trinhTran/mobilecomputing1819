package mobile.computing.ws1819.shared.type;

/*
BoundedValue is value that has 
a default and fixed minimum value called lower bound and 
a customizable maximum value called upper bound
*/
public abstract class BoundedValue<T> {
	private T lowerBound;
	private T upperBound;

	public BoundedValue() {}

	public BoundedValue(T lowerBound, T upperBound) {
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
	}

	public void setLowerBound(T lowerBound) {
		this.lowerBound = lowerBound;
	}

	public void setUpperBound(T upperBound) {
		this.upperBound = upperBound;
	}

	public T getLowerBound() {
		return lowerBound;
	}

	public T getUpperBound() {
		return upperBound;
	}

	public static class MaxValue<T> extends BoundedValue<T> {
		public MaxValue() {}
		public MaxValue(T lowerBound, T upperBound) { super(lowerBound, upperBound);}
	}

	public static class MinValue<T> extends BoundedValue<T> {
		public MinValue() {}
		public MinValue(T lowerBound, T upperBound) { super(lowerBound, upperBound);}
	}
}
