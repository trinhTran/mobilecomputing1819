package mobile.computing.ws1819.shared.type;

import static mobile.computing.ws1819.shared.constant.SmtpCommandSyntaxPattern.*;
import static mobile.computing.ws1819.shared.type.HttpMethod.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mobile.computing.ws1819.server.handler.*;
import mobile.computing.ws1819.shared.constant.HelpMessages;

public enum SmtpCommand {
	PING("PING", POST, new PingCommandHandler(), HelpMessages.PING, true, PING_SYNTAX_PATTERN, new ArrayList<>(
			Arrays.asList(SmtpStates.SESSION_NOT_INITIALIZED)
	)),

	HELO("HELO", PUT, new HeloCommandHandler(), HelpMessages.HELO, true, HELO_SYNTAX_PATTERN, new ArrayList<>(
			Arrays.asList(SmtpStates.SESSION_INITIALIED,
						SmtpStates.MAIL_TRANSACTION_INITIALIZED,
						SmtpStates.RECEIVING_RECIPENT,
						SmtpStates.RECEIVING_DATA)
	)),

	EHLO("EHLO", PUT, new EhloCommandHandler(), HelpMessages.EHLO, true, EHLO_SYNTAX_PATTERN, new ArrayList<>(
			Arrays.asList(SmtpStates.SESSION_INITIALIED,
						SmtpStates.MAIL_TRANSACTION_INITIALIZED,
						SmtpStates.RECEIVING_RECIPENT,
						SmtpStates.RECEIVING_DATA)
	)),

	MAIL("MAIL", POST, new MailCommandHandler(), HelpMessages.MAIL, true, MAIL_SYNTAX_PATTERN, new ArrayList<>(
			Arrays.asList(SmtpStates.CLIENT_SAID_HELO)
	)),

	RCPT("RCPT", PUT, new RcptCommandHandler(), HelpMessages.RCPT, true, RCPT_SYNTAX_PATTERN, new ArrayList<>(
			Arrays.asList(SmtpStates.MAIL_TRANSACTION_INITIALIZED, SmtpStates.RECEIVING_RECIPENT)
	)),

	DATA("DATA", PUT, new DataCommandHandler(), HelpMessages.DATA, true, DATA_SYNTAX_PATTERN, new ArrayList<>(
			Arrays.asList(SmtpStates.RECEIVING_RECIPENT, SmtpStates.RECEIVING_DATA)
	)),

	RSET("RSET", DELETE, new RsetCommandHandler(), HelpMessages.RSET, true, RSET_SYNTAX_PATTERN, new ArrayList<>(
			Arrays.asList(SmtpStates.values())
	)),

	VRFY("VRFY", PUT, new VrfyCommandHandler(), HelpMessages.VRFY, true, VRFY_SYNTAX_PATTERN, new ArrayList<>(
			Arrays.asList(SmtpStates.values())
	)),

	EXPN("EXPN", PUT, new ExpnCommandHandler(), HelpMessages.EXPN, true, EXPN_SYNTAX_PATTERN, new ArrayList<>(
			Arrays.asList(SmtpStates.values())
	)),

	HELP("HELP", PUT, new HelpCommandHandler(), HelpMessages.HELP, true, HELP_SYNTAX_PATTERN, new ArrayList<>(
			Arrays.asList(SmtpStates.values())
	)),

	NOOP("NOOP", PUT, new NoopCommandHandler(), HelpMessages.NOOP, true, NOOP_SYNTAX_PATTERN, new ArrayList<>(
			Arrays.asList(SmtpStates.values())
	)),

	QUIT("QUIT", DELETE, new QuitCommandHandler(), HelpMessages.QUIT, true, QUIT_SYNTAX_PATTERN, new ArrayList<>(
			Arrays.asList(SmtpStates.values())
	));

	public String name;
	public String helpMessage;
	public HttpMethod httpMethod;
	public SmtpCommandHandler commandHandler;
	public boolean enabled;
	public String commandSyntaxPattern;

	//Each SMTP command is allowed to be issued at a certain states.
	//This is a list of all states, at which the SMTP command is allowed to be issued.
	public List<SmtpStates> allowedStates;

	SmtpCommand(String name, HttpMethod httpMethod, SmtpCommandHandler commandHandler, String helpMessage, boolean enabled, String commandSyntaxPattern, List<SmtpStates> allowedStates) {
		this.name = name;
		this.httpMethod = httpMethod;
		this.commandHandler = commandHandler;
		this.enabled = enabled;
		this.helpMessage = helpMessage;
		this.commandSyntaxPattern = commandSyntaxPattern;
		this.allowedStates = allowedStates;
	}

	public String getName() {
		return name;
	}

	public HttpMethod getHttpMethod() {
		return httpMethod;
	}

	public SmtpCommandHandler getCommandHandler() {
		return commandHandler;
	}

	public boolean isEnabled() {
		return enabled;
	}
	
	public String getHelpMessage() {
		return this.helpMessage;
	}

	public boolean isAllowedAtState(SmtpStates state) {
		return allowedStates.contains(state);
	}

	public boolean isSmtpMessageSyntaxValid(String smtpMessage) {
		Matcher messagePatternMatcher = Pattern.compile(commandSyntaxPattern).matcher(smtpMessage);
		if (messagePatternMatcher.matches()) {
			return true;
		}
		return false;
	}

	public static List<SmtpCommand> getAllStandardSmtpCommand() {
		List<SmtpCommand> result = new ArrayList<>(Arrays.asList(SmtpCommand.values()));
		result.remove(SmtpCommand.PING);
		return result;
	}
	
	public static List<String> getEnabledCommandList() {
		List<String> result = new ArrayList();
		for (SmtpCommand item : SmtpCommand.getAllStandardSmtpCommand()) {
			if (item.enabled) {
				result.add(item.getName());
			}
		}
		return result;
	}
	
	public static SmtpCommand getByName(String name) {
		if (name == null || name.isEmpty())
			return null;
		for (SmtpCommand item : SmtpCommand.values()) {
			if (item.name.toUpperCase().equals(name.trim().toUpperCase())) {
				return item;
			}
		}
		return null;
	}
}
