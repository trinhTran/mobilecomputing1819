package mobile.computing.ws1819.shared.model;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mobile.computing.ws1819.shared.utils.CrossPlatformFormater;

/*
A Message object holding all require information about a HTTP request, including:
httpStatus, smtpStatus, session UUID, current state, timestamp, smtpMessageContent
The object will be seralized to JSON string at sender side, and send throgh HTTP protocol as PLAIN/TEXT.
It will then deserialized back at reveicer side.
*/
public class Message {
	private static final ObjectMapper jsonObjectMapper = new ObjectMapper();

	private String smtpMessageContent;
	private int currentSmtpStateOrder;
	private String smtpSessionUID;
	private int smtpStatus;
	private int httpStatus;
	private String timestamp;

	public Message() {
		httpStatus = 0;
		smtpStatus = 0;
		currentSmtpStateOrder = 0;
		smtpSessionUID = "";
		timestamp = "";
		smtpMessageContent = "";
	}

	public Message(int httpStatus, int smtpStatus, int currentSmtpStateOrder, String smtpSessionUID, String timestamp, String smtpMessageContent) {
		this.httpStatus = httpStatus;
		this.smtpStatus = smtpStatus;
		this.currentSmtpStateOrder = currentSmtpStateOrder;
		this.smtpSessionUID = smtpSessionUID;
		this.timestamp = timestamp;
		this.smtpMessageContent = smtpMessageContent;
	}

	public int getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}

	public int getSmtpStatus() {
		return smtpStatus;
	}

	public void setSmtpStatus(int smtpStatus) {
		this.smtpStatus = smtpStatus;
	}

	public int getCurrentSmtpStateOrder() {
		return currentSmtpStateOrder;
	}

	public void setCurrentSmtpStateOrder(int currentSmtpStateOrder) {
		this.currentSmtpStateOrder = currentSmtpStateOrder;
	}

	public String getSmtpSessionUID() {
		return smtpSessionUID;
	}

	public void setSmtpSessionUID(String smtpSessionUID) {
		this.smtpSessionUID = smtpSessionUID;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getSmtpMessageContent() {
		return smtpMessageContent;
	}

	public void setSmtpMessageContent(String smtpMessageContent) {
		this.smtpMessageContent = smtpMessageContent;
	}

	@Override
	public String toString() {
		return String.format("Message: [httpStatus: %s, smtpStatus: %s, smtpSessionUID: %s, currentSmtpStateOrder: %s, timestamp: %s, smtpMessageContent: %s]"
				, String.valueOf(httpStatus), String.valueOf(smtpStatus), smtpSessionUID, String.valueOf(currentSmtpStateOrder), timestamp, smtpMessageContent);
	}

	public String toJsonString() {
		try {
			return jsonObjectMapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static class MessageBuilder {
		
		private Message message;
		
		public MessageBuilder() {
			this.message = new Message();
		}
		
		public MessageBuilder newFromJsonString(String jsonString) throws IOException {
			this.message = jsonObjectMapper.readValue(jsonString, Message.class);
			return this;
		}
		
		public MessageBuilder setHttpStatus(int httpStatus) {
			this.message.setHttpStatus(httpStatus);
			return this;
		}

		public MessageBuilder setSmtpStatus(int smtpStatus) {
			this.message.setSmtpStatus(smtpStatus);
			return this;
		}
		
		public MessageBuilder setCurrentSmtpStateOrder(int currentSmtpStateOrder) {
			this.message.setCurrentSmtpStateOrder(currentSmtpStateOrder);
			return this;
		}
		
		public MessageBuilder setSmtpSessionUID(String smtpSessionUID) {
			this.message.setSmtpSessionUID(smtpSessionUID);
			return this;
		}
		
		public MessageBuilder setTimestamp(String timestamp) {
			this.message.setTimestamp(timestamp);
			return this;
		}
		
		public MessageBuilder setSmtpMessageContent(String smtpMessageContent) {
			this.message.setSmtpMessageContent(smtpMessageContent);
			return this;
		}
		
		public Message build() {
			return this.message;
		}
	}
}
