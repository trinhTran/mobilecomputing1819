package mobile.computing.ws1819.shared.model;

import mobile.computing.ws1819.shared.constant.ServerLimits;
import mobile.computing.ws1819.shared.type.SmtpStates;

import java.util.ArrayList;
import java.util.List;

/*
A session object is given specific to a user session, 
which will be create with a request to PING command.
A session object is identified by it's UUID,
which is generated using UUID.getUuid().
A session object will hold information about a SMTP session, including:
state table, client domain, reverse path, recipient buffer and data buffer.
*/
public class Session {

	private String uuid;
	private SmtpStates currentSmtpState;
	private String clientDomain;
	private String reverseMailBox;
	private List<String> recipentList;
	private String fromLine;
	private String toLine;
	private String subjectLine;
	private String mailBody;

	public Session() {
		recipentList = new ArrayList<>();
	}

	public Session(String uuid, SmtpStates currentSmtpState, String clientDomain,
				   String reverseMailBox, List<String> recipentList, String fromLine,
				   String toLine, String subjectLine, String mailBody) {
		this.uuid = uuid;
		this.currentSmtpState = currentSmtpState;
		this.clientDomain = clientDomain;
		this.reverseMailBox = reverseMailBox;
		this.recipentList = recipentList;
		this.fromLine = fromLine;
		this.toLine = toLine;
		this.subjectLine = subjectLine;
		this.mailBody = mailBody;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public SmtpStates getCurrentSmtpState() {
		return currentSmtpState;
	}

	public void setCurrentSmtpState(SmtpStates currentSmtpState) {
		this.currentSmtpState = currentSmtpState;
	}

	public String getClientDomain() {
		return clientDomain;
	}

	public void setClientDomain(String clientDomain) {
		this.clientDomain = clientDomain;
	}

	public String getReverseMailBox() {
		return reverseMailBox;
	}

	public void setReverseMailBox(String reverseMailBox) {
		this.reverseMailBox = reverseMailBox;
	}

	public List<String> getRecipentList() {
		return recipentList;
	}

	public void setRecipentList(List<String> recipentList) {
		this.recipentList = recipentList;
	}

	public String getFromLine() {
		return fromLine;
	}

	public void setFromLine(String fromLine) {
		this.fromLine = fromLine;
	}

	public String getToLine() {
		return toLine;
	}

	public void setToLine(String toLine) {
		this.toLine = toLine;
	}

	public String getSubjectLine() {
		return subjectLine;
	}

	public void setSubjectLine(String subjectLine) {
		this.subjectLine = subjectLine;
	}

	public String getMailBody() {
		return mailBody;
	}

	public void setMailBody(String mailBody) {
		this.mailBody = mailBody;
	}

	public boolean tryToAddNewRecipient(String recipent) {
		if (this.recipentList.size() < ServerLimits.RECIPIENT_LIST_SIZE.getUpperBound()) {
			this.recipentList.add(recipent);
			return true;
		}
		return false;
	}

	@Override
	public boolean equals(Object other) {
		Session otherSession = (Session) other;
		return this.uuid.equals(((Session) other).uuid);
	}

	@Override
	public int hashCode() {
		return this.uuid.hashCode();
	}

	public void cleanUp() {
		this.clientDomain = "";
		this.reverseMailBox = "";
		this.recipentList.clear();
		this.fromLine = "";
		this.toLine = "";
		this.subjectLine = "";
		this.mailBody = "";
	}
}
