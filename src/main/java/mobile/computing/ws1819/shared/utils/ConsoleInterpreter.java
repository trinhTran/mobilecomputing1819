package mobile.computing.ws1819.shared.utils;

import mobile.computing.ws1819.shared.log.LogEvent;
import mobile.computing.ws1819.shared.log.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
This is a help class that provide some method to interact with users.
To get the answwer for a yes/no question, a normal question or a multiple questions.
*/
public class ConsoleInterpreter {
	public static final Logger LOGGER = Logger.getLogger(ConsoleInterpreter.class);

	public static void printQuestion(String question) {
		System.out.println("\n\033[35m|~~> " + question + "\033[0m");
	}

	public static String getInputByLine() throws IOException {
		System.out.print("\033[35m<~~| ");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String answer = br.readLine();
		System.out.println("\033[0m");
		return answer;
	}

	public static String getAnswerForYesNoQuestion(String question) {
		printQuestion(question);
		String answer = "";
		while (true) {
			try {
				answer = getInputByLine();
				if (!"yes".equals(answer.trim().toLowerCase())
						&& !"no".equals(answer.trim().toLowerCase()))
				{
					System.out.println("\n\033[31mERROR: answer can only be 'yes' or 'no'. Please type again!\033[0m");
					continue;
				}
				break;
			} catch (Exception e) {
				LOGGER.error(LogEvent.create("getAnswerForYesNoQuestion", "error when trying to answer for yes/no question")
					.add("question", question).add(e));
			}
		}
		return answer;
	}

	public static String getAnswerForNormalQuestion(String question) {
		printQuestion(question);
		String answer = "";
		try {
			answer = getInputByLine();
		} catch (Exception e) {
			LOGGER.error(LogEvent.create("getAnswerForYesNoQuestion", "error when trying to answer for question")
					.add("question", question).add(e));
		}
		return answer;
	}
}
