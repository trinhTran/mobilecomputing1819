package mobile.computing.ws1819.shared.utils;

public class CrossPlatformFormater {

	/*
	Because we might in the case that the server is running on Linux and sending message
	to a Windows client using \n character, or vice versa, so this method wiwll help to
	convert of Linux EOL (\n) to WWindows EOL (\r\n) or from Windows EOL to Linux EOL.
	*/
	public static String changeToPlatformSpecificEOL(String input) {
		String result = input.replaceAll("(\r\n|\r|\n|\n\r)", System.lineSeparator());
		return result;
	}
}
