package mobile.computing.ws1819.shared.log;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/*
LogEvent class is used to construct the log string for the Logger.
*/
public class LogEvent {
	public static LogStringBuilder create(String method, String message) {
		return new LogStringBuilder(method, message);
	}

	public static class LogStringBuilder {
		private StringBuilder stringBuilder;

		public LogStringBuilder (String method, String message) {
			stringBuilder = new StringBuilder();
			stringBuilder.append(method).append(" |>").append(message);
		}

		public LogStringBuilder add(String key, String value) {
			stringBuilder.append(System.lineSeparator())
					.append("> " + key + " = " + value);
			return this;
		}

		public LogStringBuilder add(String key, Object obj) {
			stringBuilder.append(System.lineSeparator())
					.append("> "+ key + " = " + obj.toString());
			return this;
		}

		public LogStringBuilder add(String key, Object... objs) {
			String str = "> "+ key + " = [";
			List<String> objsString = Arrays.asList(objs).stream().map(obj -> obj.toString()).collect(Collectors.toList());
			str += String.join(", ",objsString) + "]";
			stringBuilder.append(System.lineSeparator()).append(str);
			return this;
		}

		public LogStringBuilder add(Throwable t) {
			StringWriter writer = new StringWriter();
			t.printStackTrace(new PrintWriter(writer));
			stringBuilder.append(System.lineSeparator() + t.getMessage() + System.lineSeparator());
			stringBuilder.append(writer.toString());
			return this;
		}

		public String toString() {
			return stringBuilder.toString();
		}
	}
}
