package mobile.computing.ws1819.shared.log;

/*
This class is an implementation of Logger.
It will print log string to Stdout with colored .
Other implementation can print it to a file or database or stream.
 */
public class StdoutLogger extends Logger{

	@Override
	public void debug(LogEvent.LogStringBuilder logBuiler) {
		if (logLevel <= LogLevel.DEBUG) {
			String logString = "DEBUG: " + className + " " + logBuiler.toString();
			this.log(logString);
		}
	}

	@Override
	public void info(LogEvent.LogStringBuilder logBuiler) {
		if (logLevel <= LogLevel.INFO) {
			String logString = "\033[32mINFO: " + className + " " + logBuiler.toString();
			this.log(logString);
		}
	}

	@Override
	public void warn(LogEvent.LogStringBuilder logBuiler) {
		if (logLevel <= LogLevel.WARN) {
			String logString = "\033[33mWARN: " + className + " " + logBuiler.toString();
			this.log(logString);
		}
	}

	@Override
	public void error(LogEvent.LogStringBuilder logBuiler) {
		if (logLevel <= LogLevel.ERROR) {
			String logString = "\033[31mERROR: " + className + " " + logBuiler.toString();
			this.log(logString);
		}
	}

	@Override
	public void fatal(LogEvent.LogStringBuilder logBuiler) {
		if (logLevel <= LogLevel.FATAL) {
			String logString = "\033[35mFATAL: " + className + " " + logBuiler.toString();
			this.log(logString);
		}
	}

	@Override
	public void log(String logString) {
		String logStr = logString + "\033[0m" + System.lineSeparator();
		System.out.println(logStr);
	}


}
