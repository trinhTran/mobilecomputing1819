package mobile.computing.ws1819.shared.log;

/*
We define five log level for multiple purpose.
When we chose a certain level, then only logs from that level above will be printed.
 */
public class LogLevel {
	public static final int DEBUG = 0;
	public static final int INFO = 1;
	public static final int WARN = 2;
	public static final int ERROR = 3;
	public static final int FATAL = 4;
}
