package mobile.computing.ws1819.shared.log;


/*
This is the base class, which provide some common methods that will be used by any logging
implementation
 */
public abstract class Logger {
	public static final Class LOGGER_IMPL = StdoutLogger.class;
	public static final int logLevel = (System.getProperty("smtp.log.level") != null) ? Integer.parseInt(System.getProperty("smtp.log.level")) : LogLevel.INFO;

	protected String className;

	public abstract void debug(LogEvent.LogStringBuilder logBuiler);
	public abstract void info(LogEvent.LogStringBuilder logBuiler);
	public abstract void warn(LogEvent.LogStringBuilder logBuiler);
	public abstract void error(LogEvent.LogStringBuilder logBuiler);
	public abstract void fatal(LogEvent.LogStringBuilder logBuiler);
	public abstract void log(String logString);

	public static Logger getLogger(Class clazz) {
		return getLogger(clazz.getName());
	}

	public static Logger getLogger(String className) {
		Logger logger = null;
		try {
			logger = (Logger) LOGGER_IMPL.newInstance();

		} catch (InstantiationException e) {
			System.err.println("ERROR: cannot initiate logger of class StdoutLogger. Will use simple logger implementation");
			e.printStackTrace();

			logger = new Logger() {
				@Override
				public void debug(LogEvent.LogStringBuilder logBuiler) {
					
				}

				@Override
				public void info(LogEvent.LogStringBuilder logBuiler) {
					//dump logger
				}

				@Override
				public void error(LogEvent.LogStringBuilder logBuiler) {
					//dump logger
				}

				@Override
				public void fatal(LogEvent.LogStringBuilder logBuiler) {

				}

				@Override
				public void warn(LogEvent.LogStringBuilder logBuiler) {
					//dump logger
				}

				@Override
				public void log(String logString) {
					//dump logger
				}
			};
		} catch (IllegalAccessException e) {
			System.err.println("ERROR: cannot initiate logger of class StdoutLogger. Will use simple logger implementation");
			e.printStackTrace();
		}
		logger.className = className;
		return logger;
	}
}
